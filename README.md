# Fluent API

ChartBuilder, LineSeriesBuilder, XAxisBuilder und YAxisBuilder

## ChartBuilder

Mit diesem Builder wird das Chart als solches bearbeitet. Es konfiguriert die Legendendarstellung, Zoom-Optionen, etc. Sämtliche Konfiguration des Charts ist optional.

**Example-Usage:**

```javascript
    this.mainChart = chartBuilder
      .withLegend("top")
      .withXYCursor("zoomXY")
      .withXZoombar()
      .withYZoombar()
      .build();
```

**Methodenübersicht:**

-- optional

* _withLegend(legendPosition: am4charts.LegendPosition)_  
Konfiguriert, dass eine Legende mit den enthaltenen Series-Definitionen dargestellt werden soll. Der Parameter _legendPosition_ definiert dabei die Position.

* _withXYCursor(type: ZoomType)_  
Mit dieser Konfiguration wird ein XY Cursor (Fadenkreuz) im Chart angezeigt. Der Parameter _type_ bestimmt dabei das Verhalten beim Selektieren eines Bereiches im Chart.

* _withXZoombar()_  
Fügt den horizontalen Zoombalken über dem Chart hinzu.

* _withYZoombar()_  
Fügt den vertikalen Zoombalken rechts vom Chart hinzu.


## YAxisBuilder

Die Klasse YAxisBuilder liefert Methoden, um die Y-Achse zu konfigurieren.

**Example-Usage:**

```javascript
import { createYAxisBuilder as yAxisBuilder } from './YAxisBuilder';
...

    const power = yAxisBuilder(chart)  
        .newValueAxis("Power [[MW]]", 0, 12)
        .applyLineSeries(
            lineSeriesBuilder()
                .withName("Power [[MW]]")
                .withColor("#04724D")
                .withValueXProperty("time")
                .withValueYProperty("power")
                .withTooltipText("{valueY.formatNumber('#.00')} MW")
                .useSeriesColorForTooltip()
                .asThickLine()
                .withData(powers)
                .getSeries()
        )
        .applySeriesColorToAxis()
        .onOpposite()
        .withNumberFormat("#.00")
        .getAxis();
...
```


**Methodenübersicht:**

* _newValueAxis(label: string, min?: number, max?: number)_  
Erstellt eine neue Werte-Achse, der Parameter _label_ wird dabei neben der Achse angezeigt.
_min_ und _max_ sind optional. Mit ihnen kann eine Achsenskalierung vorgegeben werden. Sind diese nicht vorhanden, versucht amCharts selbst einen sinnvollen Bereich für die Achse zu finden.

* _applyLineSeries(series: am4charts.LineSeries):_  
Einer Achse können mehrere Trendlinien (Series) zugewiesen werden. Eine series kann dabei mit dem _LineSeriesBuilder_ erstellt werden. Der YAxisBuilder verlangt dabei immer die Angabe mind. 1 Series, nach Konfiguration aller Pflichtfelder bietet der YAxisBuilder wieder die _applyLineSeries_ Methode an, um weitere Series der Achse zuzuweisen.

* _applySeriesColorToAxis()_  
Achsenlinien und die Achsenwerte werden in derselben Farbe dargestellt wie die zuletzt hinzugefügte  Series.

* _doNotSetAxisColor()_  
Vor allem notwendig, wenn mehrere Series zugeordnet werden. Im Gegensatz zur vorigen Methode überschreibt diese Methode keine Farbkonfiguration der y-Achse.

-- optional

* _onOpposite()_  
Mit dieser Konfiguration wird die Achse auf der rechten statt der linken Seite dargestellt.

* _applyLineSeries(series: am4charts.LineSeries)_  
siehe obige Beschreibung zu dieser Methode. Hiermit können weitere Series dieser Achse zugewiesen werden.

* _withReferenceLine(value, colorHexCode)_  
Konfiguration von Min-Max-Linien

* _withChangingLineColor(lower, colorBelow, upper, colorAbove)_  
Unterschiedliches Highlighting einer Trendline auf dieser Achse, wenn sie den Min- bzw. Max-Bereich unter- bzw. überschreitet.

* _withNumberFormat(format)_  
Definition des NumberFormats für die Achsenwerte. Für weitere Details zu Formatierungen: [Formatting Numbers](https://www.amcharts.com/docs/v4/concepts/formatters/formatting-numbers/)

* _asLogarithmic()_  
Option, um die Achse als eine logarithmische Achse zu konfigurieren.

## XAxisBuilder
Mit dieser Klasse wird die Konfiguration der X-Achse vorgenommen.

**Example-Usage:**

```javascript
    const xAxis = xAxisBuilder(chart)
        .newDateAxis()
        .withTitle("Zeit")
        .withInterval({
            "timeUnit": "second",
            "count": 1
        })
        .getAxis();
```

* _newDateAxis()_  
Zurzeit sind nur zeitliche X-Achsen unterstützt. Die Datenpunkte für die Zeitachse können dabei sowohl als Unix-Timestamp oder Date angegeben werden.

* _withTitle(title: string)_  
Die Beschriftung der x-Achse. Der Title wird unterhalb der X-Achse angezeigt.

* _withInterval(interval: ITimeInterval)_  
Definiert die Auflösung der x-Achse. Die Werte können dabei in Millisekunden, Sekunden, etc. angegeben werden.

-- optional

* _withZoomChangedEventHandler(eventHandler: any)_  
Mit dieser Option kann auf veränderten Zoom reagiert werden. Der Parameter _eventHandler_ wird dabei bei Veränderungen im Zoom ausgeführt.

* _withRanges(ranges: Range[])_  
Diese Option ermöglicht es, Bereiche der x-Achse hervorzuheben. Per default wird der Bereich in einem leichten hellen rot hinterlegt.


## LineSeriesBuilder
Mit dem LineSeriesBuilder werden Serien konfiguriert, die als Liniendiagramm dargestellt werden. Sie werden zumeist für die Darstellung von Trends genutzt.

**Example-Usage:**
```javascript
const series = lineSeriesBuilder()
    .withName("Power [[MW]]")
    .withColor("#04724D")
    .withValueXProperty("time")
    .withValueYProperty("power")
    .withTooltipText("{valueY.formatNumber('#.00')} MW")
    .useSeriesColorForTooltip()
    .asThickLine()
    .withData(powers)
    .getSeries()
```

**Methodenübersicht:**

* _withName(name)_  
Vergibt der Trendlinie einen Namen, der auch in der Legende angezeigt wird.

* _withColor(colorHexCode: string, lighten?: number)_  
Konfiguriert die Farbe für die Trendlinie. Mit dem Parameter _lighten_ kann die Deckkraft ("opacity") definiert werden. Er ist optional und per default ist die Deckkraft 1 (= 100%).

* _withValueXProperty(property)_  
Konfiguriert, welches Property des Datensets für die Darstellung der Linie entlang der X-Achse herangezogen wird.

* _withValueYProperty(property)_  
Konfiguriert, welches Property des Datensets für die Darstellung der Linie entlang der Y-Achse herangezogen wird.

-- optional

* _withData(data: any[])_  
Weist der Serie die Daten zur Darstellung zu.

* _withTooltipText(text: string)_  
Konfiguration des Tooltiptextes beim Hovern über einen Datenpunkt.

* _useSeriesColorForTooltip()_  
Per default ist der Tooltip in der Farbe Schwarz, mit dieser Option lässt er sich mit der gleichen Hintergrundfarbe darstellen, wie die Trendlinie.

* _asEventSeries<T extends Sprite>(type: { new():T })_  
Diese Option konfiguriert die Series so, dass sie Events (wie z.B. Temperaturmessungen oder Materialzugaben) in einer geometrischen Form darstellt. Die geometrische Form wird dabei über den Parameter _type_ bestimmt und kann z.B. ein Kreis oder Dreieck sein.

* _asEventSeriesWithImage(href: string)_  
Im Gegensatz zu einer geometrischen Form, kann ein Event auch als Icon dargestellt werden. Ein Icon (z.B. .svg-File) muss dabei in den Folder public kopiert werden. Als Parameter _href_ muss dann der HREF auf das Icon mitgegeben werden. Die Angabe kann dabei relativ erfolgen, so kann z.B. _./icon.svg_ angegeben werden, wenn im public-Folder ein File mit dem Namen _icon.svg_ liegt.

* _asDottedLine()_  
Die Trendlinie wird punktiert dargestellt.

* _asWideDottedLine()_  
Die Trendlinie wird punktiert mit größeren Abständen zwischen den Punkten dargestellt.

* _asDashedLine()_  
Die Trendlinie wird strichliert dargestellt.

* _asWideDashedLine()_  
Die Trendlinie wird strichliert mit größeren Abständen zwischen den Strichen dargestellt.

* _asDashDottedLine()_  
Die Trendline wird strich-punktiert dargestellt.

* _asThickLine()_  
Die Trendlinie wird fett dargestellt. Per default ist sie dünn.
**Anmerkung:** _Diese Methode lässt sich mit den jeweils anderen Darstellungsmethoden ergänzen. So kann eine Trendlinie fett, mit weniger Deckkraft und punktiert dargestellt werden._

* _asLightLine()_  
Die Trendlinie wird mit weniger Deckkraft ("opacity") dargestellt.
**Anmerkung:** _Diese Methode lässt sich mit den jeweils anderen Darstellungsmethoden ergänzen. So kann eine Trendlinie fett, mit weniger Deckkraft und punktiert dargestellt werden._


## CategoryAxisBuilder
Diese Klasse muss zusammen mit dem ColumnSeriesBuilder verwendet werden, wenn keine Trenddaten dargestellt werden sollen, sondern Balkendiagramme. Auch ein Gantt-Chart (z.B. Prozessschritte) muss unter Verwendung dieser Klassen konfiguriert werden.

**Example-Usage:**
```javascript
    categoryAxisBuilder(processStepChart)
        .newCategoryAxis("Position")
        .withCategory("name")
        .applySeries(processSeriesAct)
        .inverseAxis()
        .getAxis();
```

**Methodenübersicht:**

* _newCategoryAxis(label: string)_  
Erstellt eine neue CategoryAxis auf der y-Achse.

* _withCategory(categoryName: string)_  
Definiert den Namen des Properties aus dem Datenset, welches für die Kategorisieriung herangezogen wird.

* _applySeries(series: ColumnSeries)_  
Weist der Achse eine ColumnSeries zu. Wie bei dem YAxisBuilder können auch hier mehrere Series erstellt und einer Achse zugewiesen werden. Das kann z.b. genutzt werden, um die geplanten Prozessschritte aus dem Arbeitsplan entlang der Zeit den tatsächlich durchgeführten Prozessschritten gegenüber zu stellen.

-- optional

* _withHeight(percent: number)_  
Konfigurationsmöglichkeit für die Höhe der y-Achse innerhalb des gesamten Charts.

* _placeSeriesWithinGridLines()_  
Per default werden die Balken entlang der imaginären (hellgrauen) Linien des Charts aufgetragen.
Mit dieser Option wird das verhindert und die Balken werden innerhalb der hellgrauen Linien dargestellt.

* _inverseAxis()_  
Bei einem Gantt-Chart werden Daten üblicherweise vom ersten Prozessschritt bis zum letzten Prozessschritt zur Verfügung gestellt. amCharts funktioniert so, dass es die Kategorien entlang der y-Achse in der Reihenfolge von unten nach oben aufträgt. Das Gantt-Chart wird in diesem Fall verkehrt herum dargestellt. Man hat nun entweder die Möglichkeit das Datenset zu inversieren oder die Achse umzukehren, um das gewünschte Verhalten herzustellen.

## ColumnSeriesBuilder
Diese Klasse muss zusammen mit dem CategoryAxisBuilder verwendet werden, wenn keine Trenddaten dargestellt werden sollen, sondern Balkendiagramme. Auch ein Gantt-Chart (z.B. Prozessschritte) muss unter Verwendung dieser Klassen konfiguriert werden.

**Example-Usage:**
```javascript
    const processSeriesAct = columnSeriesBuilder()
        .withName("Process Steps")
        .withColorProperty("color")
        .withDateXFromToProperty("from", "to")
        .withCategoryYProperty("name")
        .useSeriesColorForTooltip()
        .withTooltipText("Start: {from}")
        .withDateFormat("HH:mm:ss")
        .withLabelProperty("label")
        .withHeightInPercent(40)
        .getSeries();
```

**Methodenübersicht:**

* _withName(name: string)_  
Name für die Serie. Wird auch in der Legende angezeigt.

* _withColorProperty(colorPropertyName: string)_  
Mit dieser Konfiguration ist es möglich die Farbe per Datenset zu vergeben. D.h. ein Property mit der Farbinformation kann im Datenset mitgegeben werden, um die Farbe der Balken dynamisch konfigurieren zu können.

* _withColor(color: string)_  
Im Unterschied zur vorherigen Methode, vergibt diese die Farbinformation statisch der ganzen Serie. Das bedeutet auch, dass alle Balken die gleiche Farbe erhalten.

* _withDateXFromToProperty(propertyFrom: string, propertyTo: string)_  
Definiert Start- und Endzeit für die "Breite" des Balkens entlang der x-Achse.

* _withDateXProperty(property: string)_  
Definiert einen Zeitpunkt für den Balken. Der Balken wird damit entsprechend dünn dargestellt und diese Darstellungsform kann am besten als "Pins" beschrieben werden.


* _withValueYProperty(propertyName: string)_  
Definiert ein Property für den y-Wert des Balkens. Der Balken wird so "hoch" entlang der y-Achse, wie es der y-Wert angibt.

* _withCategoryYProperty(propertyName: string)_  
Damit können Kategorien entlang der y-Achse definiert werden. Diese Option muss genutzt werden, wenn man ein Gantt-Chart darstellen will und entlang der y-Achse Kategorien angezeigt werden.

-- optional

* _withHeightInPercent(percent: number)_  
Mit dieser Option kann die "Breite" der Balken in einem Gantt-Chart verringert werden. Default ist 100 (%).

* _withDateFormat(format: string)_  
Das Datumsformat wird der Series zugewiesen und verwendet. Das Datumsformat wird z.B. bei der Anzeige des Tooltips genutzt. Für weitere Details zu [Datumsformaten](https://www.amcharts.com/docs/v4/concepts/formatters/formatting-date-time/).

* _withTooltipText(text: string)_  
Definiert was im Tooltip angezeigt werden soll.

* _useSeriesColorForTooltip()_  
Verwendet die Farbe des jeweiligen Balkens für den Tooltip. Per default ist er nicht gestyled.

* _withLabelProperty(labelPropertyName: string)_  
Zusätzlich zur "Kategorie" kann ein anzuzeigende Text innerhalb des Balkens festgelegt. Der Text ist dabei linksbündig und vertikal zentriert im Balken ausgerichtet.

* _withBulletOnTop()_  
Diese Option ist für die Darstellung von "Pins" gedacht und sollte in Kombination mit _withDateXProperty_ genutzt werden. Mit _withBulletOnTop_ wird der schmalen Linie ein Kreis am oberen Ende "aufgesetzt". Der Kreis hat die selbe Farbe wie die Linie und die Linie mit dem Kreis am oberen Ende sieht dann aus wie eine Nadel (= Pin).

* _withData(data: any[])_  
Zuweisung des Datensets an die Series.
