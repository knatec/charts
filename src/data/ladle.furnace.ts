import { ColorSet, color } from "@amcharts/amcharts4/core";

function random(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min) ) + min;
}

class Recipe {
    startDate = new Date();

    private processIntervalId?: number;
    private trendIntervalId?: number;
    private trendStartDate?: Date;
    private materialAdded: any;
    private temperatureMeasured: any;

    getPowerPoints(): any[]{
        const powers: any[] = [];
        
        var newDate = new Date(this.startDate.getTime() + (10*60*1000));
        
        var onOff = [
            { "onStart": 1, "onEnd": 100 },
            { "onStart": 200, "onEnd": 500 },
            { "onStart": 775, "onEnd": 850 },
            { "onStart": 900, "onEnd": 950 },
        ];
        function isOn(index: number): boolean {
            return onOff.find(e => index >= e.onStart && index <= e.onEnd) != null;
        }
        
        for (var i = 0; i < 1000; i++) {
            newDate = new Date(newDate.getTime() + (4*1000));
        
            var power = isOn(i) ? random(5500, 8800) : 0;
        
            powers.push({
            "time": newDate,
            "power": power / 1000
            });
        }
        return powers;
    }

    public getTemperatureSetCurve() {
        const temperatures: any[] = [];

        var newTemperatureBreakPoint = 10;
        var temperature: number = 1550;
        
        var newDate = this.startDate;
        for (var i = 0; i < 1000; i++) {
          newDate = new Date(newDate.getTime() + (4*1000));
      
          temperatures.push({
            "time": newDate,
            "temperature": temperature
          });
        
          if(i === newTemperatureBreakPoint) {
            newTemperatureBreakPoint = newTemperatureBreakPoint + random(1, 30);
            temperature = temperature + 1;
          }
        }
        return temperatures;
    }

    public getProcessSteps() {
        return [
            {
                "color": color("#D62839"),
                "from": new Date(this.startDate.getTime() + (1*60*1000)),
                "to": new Date(this.startDate.getTime() + (20*60*1000)),
                "name": "Heizen"
            }, {
                "color": color("#175676"),
                "from": new Date(this.startDate.getTime() + (20*60*1000)),
                "to": new Date(this.startDate.getTime() + (40*60*1000)),
                "name": "Legieren"
            },{
                "color": color("#5E5E5E"),
                "from": new Date(this.startDate.getTime() + (40*60*1000)),
                "to": new Date(this.startDate.getTime() + (60*60*1000)),
                "name": "Homogenisieren"
            }
        ];
    }

      
    beginProcess(processName: string, callback: any) {
        if(this.processIntervalId){
            clearInterval(this.processIntervalId);
        }

        const processSteps = this.getProcessSteps();
        const step = processSteps.find(e => e.name === processName);

        const processStart = new Date();

        if(step) {
            this.processIntervalId = setInterval(() => {
                var processProgress = {
                    "colorAct": step.color.lighten(0.7),
                    "fromAct": processStart,
                    "toAct": new Date(),
                    "name": processName
                };

                callback(processProgress);
            }, 500);
        }
    }

    stopProcess() {
        clearInterval(this.processIntervalId);
    }

    beginTrend(newTemperatureCallback: any, newPowerCallback: any, materialEventCallback: any, temperatureMeasurementCallback: any) {
        if(this.trendIntervalId)
            return;

        this.trendStartDate = new Date();
        this.trendIntervalId = setInterval(() => {
            const currentDate = new Date();
            newTemperatureCallback({
                "temperatureAct": random(1550, 1600),
                "time": currentDate
            });
            
            newPowerCallback({
                "powerAct": random(7, 10),
                "time": currentDate
            });

            if(currentDate.getTime() - this.trendStartDate!.getTime() > (10*1000) && !this.materialAdded){
                materialEventCallback(this.materialAdditionAct("ALU-Griess (40)", currentDate));
                this.materialAdded = true;
            }

            if(currentDate.getTime() - this.trendStartDate!.getTime() > (15*1000) && !this.temperatureMeasured) {
                temperatureMeasurementCallback(this.temperatureMeasurementAct(1588, currentDate));
                this.temperatureMeasured = true;
            }
        }, 1000);
    }

    stopTrend() {
        clearInterval(this.trendIntervalId);
        this.trendIntervalId = undefined;
        this.trendStartDate = undefined;
        this.temperatureMeasured = false;
        this.materialAdded = false;
    }

    getData() {
        const powerTrend  = this.getTemperatureSetCurve();
        var mergedPoints = this.getPowerPoints().map((item, i) => Object.assign({}, item, powerTrend[i]));
    
        mergedPoints.splice(150, 0, this.materialAddition("ALU-Griess (40)", mergedPoints[150].time));
        return mergedPoints;
    }

    private temperatureMeasurementAct(yValue: number, time: any): any {
        return { "time": time, "temperatureMeasurementInfoAct": `Temperatur: ${yValue} °C`, "yTempAct": yValue, "value": 100 };
    }

    private materialAddition(materials: string, timestamp: Date): any {
        return { "time": timestamp, "materialevent": `${materials}`, "y": 1650, "value": 100 };
    }

    private materialAdditionAct(materials: string, timestamp: Date): any {
        return { "time": timestamp, "materialeventAct": `${materials}`, "yAct": 1650, "value": 100 };
    }

}

export default Recipe;
