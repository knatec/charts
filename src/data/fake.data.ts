import { Range } from "../components/XAxisBuilder";

var startDate = new Date();


const powerMeasuresBeforeFirstTempValue = 120;

function getTemperatures(): any[]{
  const temperatures: any[] = [];

  var newTemperatureBreakPoint = 10;
  var temperature: number = 1550;
  
  var newDate = startDate;
  for (var i = 0; i < 1000; i++) {
    newDate = new Date(newDate.getTime() - (4*1000));

    temperatures.push({
      "time": newDate,
      "temperature": i < 120 ? 0 : temperature
    });
  
    if(i === newTemperatureBreakPoint) {
      newTemperatureBreakPoint = newTemperatureBreakPoint + random(1, 30);
      temperature = temperature+1;
    }
  }
  return temperatures;
}

function getLogarithmicData(): any[]{
  const logarithmicData: any[] = [];

  var newDate = startDate;
  for (var i = 0; i < 1000; i++) {
    newDate = new Date(newDate.getTime() - (4*1000));

    logarithmicData.push({
        "time": newDate,
        "logValue": getRandomLogValue()
      });
  }

  return logarithmicData;
}

function getRandomLogValue(): number {
  return random(1, 10000);
}

function getPowerPoints(): any[]{
  const powers: any[] = [];

  var newDate = new Date(startDate.getTime() - (10*60*1000));

  var onOff = [
    { "onStart": 0, "onEnd": 100 },
    { "onStart": 200, "onEnd": 500 },
    { "onStart": 775, "onEnd": 850 },
    { "onStart": 900, "onEnd": 950 },
  ];
  function isOn(index: number): boolean {
    return onOff.find(e => index >= e.onStart && index <= e.onEnd) != null;
  }

  for (var i = 0; i < 1000 + powerMeasuresBeforeFirstTempValue; i++) {
    newDate = new Date(newDate.getTime() - (4*1000));

    var power = isOn(i) ? random(5500, 8800) : 0;

    powers.push({
      "time": newDate,
      "power": power / 1000
    });
  }
  return powers;
}

function getProcessRanges(): Range[] {
  return [{
    start: new Date(startDate.getTime() - (12 * 60 * 1000)),
    end: new Date(startDate.getTime() - (10 * 60 * 1000))
  }, {
    start: new Date(startDate.getTime() - (32 * 60 * 1000)),
    end: new Date(startDate.getTime() - (18 * 60 * 1000))
  }, {
    start: new Date(startDate.getTime() - (43.2 * 60 * 1000)),
    end: new Date(startDate.getTime() - (43 * 60 * 1000))
  }];
}

function random(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min) ) + min;
}

function mergePoints(arr1: any[], arr2: any[], arr3: any[]) : any[] {
  var mergedPoints = arr1.map((item, i) => Object.assign({}, item, arr2[i], arr3[i]));
  
  // mergedPoints = mergedPoints.map((item, i) => Object.assign({}, item, { "time": mergedPoints[i].time, "y": 1800, "value": 100}));
  
  mergedPoints.splice(30, 0, materialAddition("ALU-Griess", new Date(mergedPoints[30].time.getTime() - (2 * 1000))));
  mergedPoints.splice(330, 0, materialAddition("Ferro Mangan carbure (451)\nFerro Tungsten (15)", mergedPoints[330].time));
  mergedPoints.splice(420, 0, materialAddition("Ferro Vanadium 80% (10)", mergedPoints[420].time));
  mergedPoints.splice(800, 0, materialAddition("Legierungskohle (24)", mergedPoints[800].time));
  
  console.info(mergedPoints);
  
  return mergedPoints.reverse();

  function materialAddition(materials: string, timestamp: Date): any {
    return { "time": timestamp, "event": `${materials}`, "y": 1900, "value": 100, "number": 20, "online": true };
  }
}

const points = mergePoints(getTemperatures(), getPowerPoints(), getLogarithmicData());
const processRanges = getProcessRanges();

function startLiveData(newDataAvailable: any): number {
  return setInterval(() => {
    const currentTime = new Date();
    newDataAvailable({ temperature: random(1400, 1600), time: currentTime.getTime() });
  }, 100);
}

function zoomData(start: Date, end: Date, resolution: 500) {
  console.info(`here we could fetch data with higher resolution. new range: ${start} - ${end}`);
}

const sinusPoints = getSinusCurve().reverse();

function getSinusCurve(): any[] {
  const temperatures: any[] = [];

  var newDate = startDate;
  for (var i = 0; i < 1000; i++) {
    newDate = new Date(newDate.getTime() - (1000));

    temperatures.push({
      "time": newDate,
      "sinus": Math.sin(i / Math.PI)
    });
  }
  return temperatures;
}

function startLiveSinus(newDataAvailable: any): number {
  var lastIndex = 1000;
  return setInterval(() => {
    lastIndex = lastIndex + 1;
    const currentTime = new Date();
    newDataAvailable({ sinus: Math.sin(lastIndex / Math.PI), time: currentTime.getTime() }, 100);
  }, 100);
}

export { startDate, sinusPoints, points, processRanges, startLiveData, startLiveSinus, zoomData };