import { color } from "@amcharts/amcharts4/core";

function getTemperatures(start: Date): any[] {
    const temperatures: any[] = [];

    var newTemperatureBreakPoint = 10;
    var temperature: number = 1640;

    var newDate = start;
    for (var i = 0; i < 1000; i++) {
        newDate = new Date(newDate.getTime() + (4*1000));

        temperatures.push({
            "time": newDate,
            "temperature": temperature
        });

        if(i === newTemperatureBreakPoint) {
            newTemperatureBreakPoint = newTemperatureBreakPoint + random(1, 30);
            temperature = temperature - random(1, 3);
        }
    }
    return temperatures;
}
  
function getTemperatureMeasurements(start: Date) {
    var newDate = start;

    for (var i = 0; i < 1000; i++) {
        newDate = new Date(newDate.getTime() + (4*1000));

        if(i % 150 === 0) {
            temperatures.push({
                "time": newDate,
                "temperatureMeasurement": random(1540, 1650)
            });
        }
    }
    return temperatures;

}

function getPowerPoints(startDate: Date): any[]{
    var onOff = [
        { "onStart": 0, "onEnd": 900 }
    ];
    function isOn(index: number): boolean {
        return onOff.find(e => index >= e.onStart && index <= e.onEnd) != null;
    }

    const powers: any[] = [];
  
    var newDate = startDate;
  
    var lastPower = 0;
    var startPower = random(6000, 6200);

    for (var i = 0; i < 1000; i++) {
        newDate = new Date(newDate.getTime() + (4*1000));
    
        var power;
        if(isOn(i)) {
            power = linearCalc(900, i, 6300, startPower);
        } else {
            power = lastPower - 1;
        }
        
      
        powers.push({
            "time": newDate,
            "power": power / 1000
        });
        lastPower = power;
    }
    return powers;
}
  
function linearCalc(x: number, targetX: number, y: number, d: number): number {
    return ((y - d) / x) * targetX + d;
}

function random(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min) ) + min;
}

function getMaintenance(startDate: Date): any[] {
    var col = color("#537A5A");

    return [
        {
           "color": col,
           "from": new Date(startDate.getTime() + (1 * 60 * 60 * 1000)),
           "to": new Date(startDate.getTime() + (1.2 * 60 * 60 * 1000)),
           "name": "Maintenance"
       },{
           "color": col,
           "from": new Date(startDate.getTime() + (3 * 60 * 60 * 1000)),
           "to": new Date(startDate.getTime() + (3.9 * 60 * 60 * 1000)),
           "label": "Ki",
           "name": "Maintenance"
       },{
           "color": col,
           "from": new Date(startDate.getTime() + (5 * 60 * 60 * 1000)),
           "to": new Date(startDate.getTime() + (5.8 * 60 * 60 * 1000)),
           "label": "Ki",
           "name": "Maintenance"
       },{
           "color": col,
           "from": new Date(startDate.getTime() + (7 * 60 * 60 * 1000)),
           "to": new Date(startDate.getTime() + (7.2 * 60 * 60 * 1000)),
           "name": "Maintenance"
       },{
           "color": col,
           "from": new Date(startDate.getTime() + (10.1 * 60 * 60 * 1000)),
           "to": new Date(startDate.getTime() + (11 * 60 * 60 * 1000)),
           "label": "Ki",
           "name": "Maintenance"
       }
   ];
}
function getPreHeater(startDate: Date): any[] {
    var col = color("#FF9914");

    return [
        {
           "color": col,
           "from": new Date(startDate.getTime() - (6.8 * 60 * 60 * 1000)),
           "to": new Date(startDate.getTime()),
           "label": "Feuer 4 - 828.3 min",
           "name": "Preheater"
       },{
           "color": col,
           "from": new Date(startDate.getTime() + (8 * 60 * 60 * 1000)),
           "to": new Date(startDate.getTime() + (8.3 * 60 * 60 * 1000)),
           "name": "Preheater"
       },{
           "color": col,
           "from": new Date(startDate.getTime() + (14 * 60 * 60 * 1000)),
           "to": new Date(startDate.getTime() + (14.3 * 60 * 60 * 1000)),
           "name": "Preheater"
       }
   ];
}

function getTransport(startDate: Date, endDate: Date): any[] {
    var col = color("#247BA0");

    const transport: any[] = [];

    var date = startDate;
    for(var i = 0; i < 40 && date.getTime() < endDate.getTime(); i++) {
        var toDate = new Date(date.getTime() + random(1 * 60 * 1000, 45 * 60 * 1000));
        transport.push({
            "color": col,
            "from": new Date(date.getTime()),
            "to": toDate,
            "name": "Transport"
        });

        date = new Date(toDate.getTime() + random(60 * 1000, 60 * 60 * 1000));
    }

    return transport;
}

function getProcess(startDate: Date, endDate: Date): any[] {
    var col = color("#BB342F");

    const process: any[] = [];

    var lengths = [{
        "from": 1 * 60 * 1000,
        "to": 5 * 60 * 1000,
        "pause": 10 * 60 * 1000,
    }, {
        "from": 10 * 60 * 1000,
        "to": 30 * 60 * 1000,
        "label": "P",
        "pause": 10 * 60 * 1000,
    }, {
        "from": 10 * 60 * 1000,
        "to": 30 * 60 * 1000,
        "label": "S",
        "pause": 60 * 60 * 1000,
    }]
    
    var date = new Date(startDate.getTime() + 5 * 60 * 1000);

    var lengthIndex = 0;

    for(var i = 0; i < 40 && date.getTime() < endDate.getTime(); i++) {
        var length = lengths[lengthIndex];
        var toDate = new Date(date.getTime() + random(length.from, length.to));
        process.push({
            "color": col,
            "from": new Date(date.getTime()),
            "to": toDate,
            "label": length.label,
            "name": "Process"
        });

        date = new Date(toDate.getTime() + length.pause);
        lengthIndex++;

        if(lengthIndex > lengths.length - 1) {
            lengthIndex = 0;
        }
    }

    return process;
}

function getWaitingData(startDate: Date): any[] {
    return [{
            "color": color("#2F2D2E"),
            "from": new Date(startDate.getTime() - (7 * 60 * 60 * 1000)),
            "to": new Date(startDate.getTime() - (6.8 * 60 * 60 * 1000)),
            "label": "Z",
            "name": "Waiting"
        }, {
            "color": color("#2F2D2E"),
            "from": new Date(startDate.getTime() + (6 * 60 * 60 * 1000)),
            "to": new Date(startDate.getTime() + (6.05 * 60 * 60 * 1000)),
            "label": "Z",
            "name": "Waiting"
        }, {
            "color": color("#2F2D2E"),
            "from": new Date(startDate.getTime() + (6.6 * 60 * 60 * 1000)),
            "to": new Date(startDate.getTime() + (6.7 * 60 * 60 * 1000)),
            "label": "Z",
            "name": "Waiting"
        }, {
            "color": color("#2F2D2E"),
            "from": new Date(startDate.getTime() + (14 * 60 * 60 * 1000)),
            "to": new Date(startDate.getTime() + (14.5 * 60 * 60 * 1000)),
            "label": "Z",
            "name": "Waiting"
        }, {
            "color": color("#2F2D2E"),
            "from": new Date(startDate.getTime() + (16 * 60 * 60 * 1000)),
            "to": new Date(startDate.getTime() + (16.12 * 60 * 60 * 1000)),
            "label": "Z",
            "name": "Waiting"
        }, {
            "color": color("#2F2D2E"),
            "from": new Date(startDate.getTime() + (17.7 * 60 * 60 * 1000)),
            "to": new Date(startDate.getTime() + (17.8 * 60 * 60 * 1000)),
            "label": "Z",
            "name": "Waiting"
        }];
}

var temperatures: any[] = [];
var temperatureMeasurements: any[] = [];

for(var idx = 0; idx < 10 ; idx++) {
    var startDate = temperatures.length === 0 ? new Date() : new Date(temperatures[temperatures.length - 1].time.getTime() + (60 * 60 * 1000));
    const temp = 
        [{ "time": startDate, "temperature": 0 }]
            .concat(getTemperatures(new Date(startDate.getTime() + 4000)));

    temp.push({"time": new Date(temp[temp.length - 1].time.getTime() + 4000), "temperature": 0 });
    temperatures = temperatures.concat(temp);

    temperatureMeasurements = temperatureMeasurements.concat(getTemperatureMeasurements(startDate));
}

var powers: any[] = [];
for(var idx = 0; idx < 10 ; idx++) {

    var startDate = powers.length === 0 ? new Date() : new Date(powers[powers.length - 1].time.getTime() + (60 * 60 * 1000));

    if(powers.length === 0) {
        var start = new Date(startDate.getTime() - 7 * 60 * 60 * 1000);

        var count = (startDate.getTime() - start.getTime()) / 60000;

        var startPower = 3000;
        var targetPower = 4200;

        var k = (targetPower - startPower) / count;

        var i = 0;

        while(start.getTime() < startDate.getTime()) {

            var x = i;
            var y = k * x + startPower;
            
            powers.push({ "time": start, "power": y / 1000 });
            start = new Date(start.getTime() + 60 * 1000);
            i++;
        }
    } else {
        var start = new Date(powers[powers.length - 1].time);

        var lastPower = powers[powers.length - 1].power * 1000;
        
        var count = (startDate.getTime() - start.getTime()) / 4000;
        var targetPower = random(4500, 5400);

        var substract = (lastPower - targetPower) / count;

        while(start.getTime() < startDate.getTime()) {
            lastPower -= (substract + random(0, 2));
            powers.push({ "time": start, "power": lastPower / 1000 });
            start = new Date(start.getTime() + 4000);
        }
    }

    powers = powers.concat(getPowerPoints(startDate));
}

var startDate = new Date();
const steps = getWaitingData(startDate)
                .concat(getMaintenance(startDate))
                .concat(getPreHeater(startDate))
                .concat(getTransport(startDate, temperatures[temperatures.length - 1].time))
                .concat(getProcess(startDate, temperatures[temperatures.length - 1].time));


                

export { temperatures, powers, steps, temperatureMeasurements }