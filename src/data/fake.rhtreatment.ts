import { sign } from "crypto";

const startDate = new Date();


function random(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min) ) + min;
  }


/**
 * Returns real measurements of degassing unit.
 *
 * @return {any} array containing real measurements
 */
function getRealExhaustMeasurements(): any[]{
  const measurements: any[] = [];
  var measCO: any[] = [];
  var measCO2: any[] = [];
  var exhaustAnaCO: any[] = [];
  var exhaustAnaCO2: any[] = [];
  var exhaustFlowRate: any[] = [];
  var exhaustAbsDecarb: any[] = [];
  var exhaustDecarbVel: any[] = [];
  var exhaustSignal: any[] = [];
  var flowrate: number[] = [];
  var absDecarb: number[] = [];
  var decarbVel: number[] = [];
  var signal: number[] = [];
  var mLength;
  var newDate = startDate;

  measCO = [0.06, 0.07 ,0.07 ,0.07 ,1.20 ,2.73
           ,1.86 ,1.14 ,3.98 ,23.89 ,41.32 ,49.05
           ,53.14 ,54.97 ,55.38 ,55.23 ,54.77 ,54.49
           ,53.80 ,52.69 ,50.89 ,48.25 ,45.27 ,42.66
           ,41.32 ,40.83 ,39.87 ,38.63 ,37.16 ,35.52
           ,34.01 ,32.71 ,31.70 ,30.37 ,28.63 ,27.34
           ,26.56 ,25.96 ,24.71 ,22.58 ,20.73 ,19.07
           ,18.47 ,18.41 ,18.16 ,17.16 ,15.57 ,14.47
           ,13.57 ,12.81 ,12.17 ,11.61 ,11.17 ,10.76
           ,10.33 ,9.99 ,9.67 ,9.32 ,8.85 ,8.31
           ,7.85 ,7.48 ,7.18 ,6.86 ,6.49 ,6.22
           ,6.02 ,5.79 ,5.53 ,5.31 ,5.11 ,4.96
           ,4.82 ,4.64 ,4.45 ,4.29 ,4.16 ,4.03
           ,3.92 ,3.80 ,3.69 ,3.59 ,3.52 ,3.46
           ,3.39 ,3.37 ,3.30 ,3.13 ,2.72 ,2.05
           ,1.50 ,1.07 ,0.80 ,0.75 ,0.67 ,0.51
           ,0.60 ,0.83 ,1.08 ,1.42 ,1.83 ,2.18
           ,2.37];

  measCO2 = [0.09, 0.10, 0.10 ,0.10 ,0.12 ,0.68
            ,0.70 ,0.48 ,0.50 ,3.54 ,6.24 ,7.22
            ,7.03 ,6.69 ,6.47 ,6.35 ,6.20 ,6.20
            ,6.22 ,6.28 ,6.24 ,6.02 ,5.75 ,5.46
            ,5.45 ,5.33 ,5.19 ,5.10 ,5.04 ,4.99
            ,4.93 ,4.82 ,4.65 ,4.40 ,4.07 ,3.80
            ,3.53 ,3.22 ,3.00 ,3.06 ,3.10 ,2.93
            ,2.71 ,2.54 ,2.46 ,2.42 ,2.36 ,2.30
            ,2.25 ,2.20 ,2.15 ,2.10 ,2.07 ,2.04
            ,2.00 ,1.97 ,1.94 ,1.91 ,1.87 ,1.80
            ,1.74 ,1.69 ,1.66 ,1.63 ,1.59 ,1.56
            ,1.54 ,1.52 ,1.49 ,1.47 ,1.45 ,1.43
            ,1.41 ,1.40 ,1.38 ,1.36 ,1.34 ,1.32
            ,1.31 ,1.29 ,1.27 ,1.26 ,1.25 ,1.25
            ,1.25 ,1.25 ,1.25 ,1.22 ,1.15 ,0.93
            ,0.65 ,0.59 ,0.81 ,1.18 ,1.43 ,1.56
            ,1.29 ,0.88 ,0.66 ,0.63 ,0.78 ,0.98
            ,1.10] ;


    flowrate = [ 646, 3853, 6000, 6000, 6000, 6000
                ,6000, 6000, 6000, 6000, 6000, 6000
                ,6000, 4674, 2890, 1916, 1379, 1095
                ,876, 755, 657, 613, 602, 558,613
                ,591, 558, 558, 569, 547, 536
                ,514, 504, 493, 482, 514, 460
                ,460, 460, 449, 482, 460, 449
                ,405, 438, 405, 427, 394, 405
                ,383, 405, 372, 372, 372, 372
                ,372, 350, 427, 383, 350, 350
                ,372, 350, 350, 350, 350, 350
                ,350, 383, 350, 317, 350, 394
                ,350, 383, 350, 350, 350, 350
                ,372, 383, 405, 372, 372, 350
                ,350, 383, 350, 405, 372, 383
                ,383, 394, 350, 350, 372, 383
                ,383];

    absDecarb = [ 0.0162, 0.0485, 0.0538, 0.0653, 0.0552, 0.0575
                , 0.0513, 0.1153, 0.3086, 0.8534, 1.5163, 1.3157
                , 2.0262, 1.5744, 1.0239, 0.7472, 0.477, 0.2903
                , 0.3048, 0.2333, 0.22, 0.1478, 0.1776, 0.1754
                , 0.1712, 0.1762, 0.1175, 0.1417, 0.1522, 0.1276
                , 0.1315, 0.088, 0.1025, 0.1041, 0.088, 0.0978
                , 0.0607, 0.072, 0.0748, 0.063, 0.0712, 0.0482
                , 0.0576, 0.0547, 0.0511, 0.0491, 0.0357, 0.039
                , 0.0419, 0.0344, 0.0381, 0.0241, 0.0287, 0.0303
                , 0.0266, 0.0281, 0.0184, 0.0293, 0.0228, 0.022
                , 0.0155, 0.0216, 0.0174, 0.0183, 0.016, 0.0139
                , 0.0133, 0.0157, 0.0154, 0.0153, 0.01, 0.0138
                , 0.0165, 0.0111, 0.003, 0.0003, 0, 0
                , 0, 0, 0, 0, 0, 0
                , 0, 0, 0.0002, 0.0038, 0.0101, 0.0128 
                , 0.0002, 0.0002, 0.0002, 0.0038, 0.0101, 0.0128 
                , 0];

    decarbVel = [-0.3951, -1.2427, -1.333, -1.593, -1.368, -1.4022
                , -1.3147, -2.7672, -7.5253, -21.1597, -36.9779, -33.7463
                , -49.4132, -39.6966, -25.3868, -18.2223, -11.8265, -7.1981
                , -7.5581, -5.7852, -5.3657, -3.7896, -4.555, -4.2768
                , -4.2446, -4.297, -3.0133, -3.5143, -3.7121, -3.1637
                , -3.2058, -2.257, -2.5415, -2.5384, -2.1819, -2.3848
                , -1.5556, -1.7844, -1.8242, -1.5628, -1.7368, -1.2366
                , -1.4286, -1.3351, -1.2672, -1.1976, -0.9167, -0.9665
                , -1.0215, -0.8532, -0.9294, -0.6192, -0.711, -0.7389
                , -0.6604, -0.6851, -0.4729, -0.7145, -0.5551, -0.5374
                , -0.3899, -0.5448, -0.425, -0.4458, -0.3906, -0.3493
                , -0.3304, -0.3893, -0.3808, -0.3792, -0.2563, -0.3468
                , -0.4021, -0.2744, -0.0727, -0.0074, 0, 0
                , 0, 0, 0, 0, 0, 0
                , 0, 0, -0.0052, -0.0924, -0.2505, -0.3121
                , 0, 0, -0.0052, -0.0924, -0.2505, -0.3121
                , 0];

      signal = [0 , 0 , 0 , 0 , 0 , 0
              , 0 , 0 , 0 , 0 , 0 , 0
              , 0 , 0 , 0 , 0 , 0 , 0
              , 0 , 0 , 0 , 0 , 0 , 0
              , 0 , 0 , 0 , 0 , 0 , 0
              , 0 , 0 , 0 , 0 , 0 , 0
              , 0 , 0 , 0 , 0 , 0 , 0
              , 0 , 0 , 0 , 0 , 0 , 0
              , 0 , 0 , 0 , 0 , 0 , 0
              , 0 , 0 , 0 , 0 , 1 , 1
              , 1 , 1 , 1 , 1 , 1 , 1
              , 1 , 1 , 1 , 1 , 1 , 1
              , 1 , 1 , 1 , 1 , 1 , 1
              , 1 , 1 , 1 , 1 , 1 , 1
              , 1 , 1 , 1 , 1 , 1 , 1
              , 1 , 1 , 0 , 1 , 1 , 1
              ,1];

      for(var i = 0; i<signal.length; i++){
        if(signal[i] == 0){
          signal[i] = 10;
        }
      }
      signal = signal.map(x => x*-1);
  
  mLength = measCO.length;
  for(var i = 0; i<mLength; i++){
    newDate = new Date(newDate.getTime() + (10*1000));
    measurements.push({
                      "time": newDate,
                      "measCO": measCO[i],
                      "measCO2": measCO2[i],
                      "flowrate": flowrate[i],
                      "absDecarb": absDecarb[i],
                      "decarbVel": decarbVel[i],
                      "signal": signal[i]
                      });

      exhaustAnaCO.push({
                      "time": newDate,
                      "measCO": measCO[i]
      })

      exhaustAnaCO2.push({
                      "time": newDate,
                      "measCO2": measCO2[i]
      })

      exhaustFlowRate.push({
                      "time": newDate,
                      "flowrate": flowrate[i]
      })

      exhaustAbsDecarb.push({
                      "time": newDate,
                      "absDecarb": absDecarb[i]
      })

      exhaustDecarbVel.push({
                      "time": newDate,
                      "decarbVel": decarbVel[i]
      })

      exhaustSignal.push({
                      "time": newDate,
                      "signal": signal[i]
      })

  }
  
  return [exhaustAnaCO, exhaustAnaCO2, exhaustFlowRate,
          exhaustAbsDecarb, exhaustDecarbVel, exhaustSignal]
}


/**
 * Returns material addition
 *
 * @return {any} array containing date and corresponing carbon content.
 */
function materialAddition(material: string): any[]{
  const addition: any[] = [];
  var newDate = startDate;

  addition.push({
                "time": new Date(newDate.getTime() + (400*1000)),
                "materialevent": `${material}`,
                "y": -7,
                "value": 246
  });
  return addition;
}


/**
 * Returns declining e-function for degassing treatment.
 *
 * @return {any} array containing date and corresponing carbon content.
 */
function getCarbonContentHeat(): any[]{
  const measurements: any[] = [];
    
  var newDate = startDate;
  var k: number = -0.0042754;
  var cStart: number = 320;
  var cContent: number;
  var t: number;

  for(var i = 0; i < 80; i++){
    newDate = new Date(newDate.getTime() + (10*1000));
    t = i * 10;

    cContent = cStart * Math.exp(k*t) + random(-10, 10); 
    //cContent = cStart;
    measurements.push({
      "time": newDate,
      "content": cContent
    });
  }
    return measurements;
}



function getCarbonContent(): any[]{
    const temperatures: any[] = [];
  
    var newTemperatureBreakPoint = 10;
    var temperature: number = 70;
    
    var newDate = startDate;
    for (var i = 0; i < 1000; i++) {
      newDate = new Date(newDate.getTime() - (4*1000));
  
      temperatures.push({
        "time": newDate,
        "content": temperature + random(0, 50)
      });
    
    }
    return temperatures;
  }


  const CarbonContent = getRealExhaustMeasurements();
  const anaCO = getRealExhaustMeasurements()[0];
  const anaCO2 = getRealExhaustMeasurements()[1];
  const flowRate = getRealExhaustMeasurements()[2];
  const absDecarb = getRealExhaustMeasurements()[3];
  const decarbVel = getRealExhaustMeasurements()[4];
  const signal = getRealExhaustMeasurements()[5];
  const materialAdd = materialAddition("AL_STU");
  
  export { anaCO, anaCO2, flowRate, absDecarb, decarbVel, signal, materialAdd };