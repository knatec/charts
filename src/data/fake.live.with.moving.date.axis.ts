import { ColorSet, color } from "@amcharts/amcharts4/core";

function random(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min) ) + min;
}

class Recipe {
    private startDate = new Date();

    private processIntervalId?: number;
    private trendIntervalId?: number;
    private rememberedEndTime: Date = new Date();

    public getTemperatureData() {
        const temperatures: any[] = [];

        var newTemperatureBreakPoint = 10;
        var temperature: number = 1550;
        
        var newDate = this.startDate;
        for (var i = 0; i < 1000; i++) {
          newDate = new Date(newDate.getTime() + (4*1000));
      
          temperatures.push({
            "X": newDate,
            "Y": temperature
          });
        
          if(i === newTemperatureBreakPoint) {
            newTemperatureBreakPoint = newTemperatureBreakPoint + random(1, 30);
            temperature = temperature + 1;
          }
        }

        console.log(temperatures);
        this.rememberedEndTime = newDate;
        return temperatures;
    }

    public getProcessSteps() {
        return [
            {
                "color": color("#D62839"),
                "from": new Date(this.startDate.getTime() + (1*60*1000)),
                "to": new Date(this.startDate.getTime() + (20*60*1000)),
                "name": "Heizen"
            }, {
                "color": color("#175676"),
                "from": new Date(this.startDate.getTime() + (20*60*1000)),
                "to": new Date(this.startDate.getTime() + (40*60*1000)),
                "name": "Legieren"
            },{
                "color": color("#5E5E5E"),
                "from": new Date(this.startDate.getTime() + (40*60*1000)),
                "to": new Date(this.startDate.getTime() + (60*60*1000)),
                "name": "Homogenisieren"
            }
        ];
    }

      
    beginProcess(processName: string, callback: any) {
        if(this.processIntervalId){
            clearInterval(this.processIntervalId);
        }

        const processSteps = this.getProcessSteps();
        const step = processSteps.find(e => e.name === processName);

        const processStart = new Date();

        if(step) {
            this.processIntervalId = setInterval(() => {
                var processProgress = {
                    "colorAct": step.color.lighten(0.7),
                    "fromAct": processStart,
                    "toAct": new Date(),
                    "name": processName
                };

                callback(processProgress);
            }, 500);
        }
    }

    stopProcess() {
        clearInterval(this.processIntervalId);
    }

    beginTrend(newTemperatureCallback: any, interval: number, distanceOfPointsInSeconds: number) {
        if(this.trendIntervalId)
            return;

        let i = 0;
        this.trendIntervalId = setInterval(() => {
            const N = Date.now();
            const E = this.rememberedEndTime!.getTime();
            const S = this.startDate.getTime();

            newTemperatureCallback({
                "X": new Date(E + (N - S) + (i * distanceOfPointsInSeconds * 1000)).getTime(),
                "Y": random(1550, 1600)
            });
            i++;
        }, interval);
    }

    stopTrend() {
        clearInterval(this.trendIntervalId);
        this.trendIntervalId = undefined;
    }
}

export default Recipe;
