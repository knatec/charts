import React from 'react';
import './App.css';
import AmCharts from "./components/AmChart";
import AmChartRHTreatments from "./components/AmChartRHTreatments";
import AmChartLiveTrend from "./components/AmChartLiveTrend";
import AmChartLiveWithMovingDateAxis from "./components/AmChartLiveWithMovingDateAxis";
import { Navbar, Nav, Container, NavItem } from "react-bootstrap";
import {
  BrowserRouter as Router,
  Route,
  HashRouter,
  NavLink
} from "react-router-dom";
import AmChartProcessSteps from './components/AmChartProcessSteps';
import AmChartLadleTreatments from './components/AmChartLadleTreatments';
import styled from 'styled-components';


class App extends React.Component{
  render() {
    return <HashRouter>
      <Navbar bg="primary" variant="dark"  expand="lg">
        <Navbar.Brand href="/">Charts</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
             <NavLink to="/"><Link>amCharts</Link></NavLink>
             <NavLink to="/live-sinus"><Link>Live Sinus</Link></NavLink>
             <NavLink to="/process-steps"><Link>Recipe Instructions</Link></NavLink>
             <NavLink to="/ladle-treatments"><Link>Ladle Treatments</Link></NavLink>
             <NavLink to="/rh-treatments"><Link>RH Treatments</Link></NavLink>
             <NavLink to="/live-with-moving-date-axis"><Link>Live with Moving Axis</Link></NavLink>
           </Nav>
        </Navbar.Collapse>
      </Navbar>

      <Container fluid className="p-4">
          <Route exact component={AmCharts} path="/" />
          <Route exact component={AmChartLiveTrend} path="/live-sinus" />
          <Route exact component={AmChartProcessSteps} path="/process-steps" />
          <Route exact component={AmChartLadleTreatments} path="/ladle-treatments" />
          <Route exact component={AmChartRHTreatments} path="/rh-treatments" />
          <Route exact component={AmChartLiveWithMovingDateAxis} path="/live-with-moving-date-axis" />
      </Container>
    </HashRouter>
  }
}

const Link = styled.span`
  color: #fff;
  padding: 10px;
`;

export default App;