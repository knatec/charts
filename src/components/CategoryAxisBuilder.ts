import { XYChart, CategoryAxis, ColumnSeries } from "@amcharts/amcharts4/charts";
import { DateFormatter, percent } from "@amcharts/amcharts4/core";

function categoryAxisBuilder(chart: XYChart): WithName {
    return new CategoryAxisBuilder(chart);
}

class CategoryAxisBuilder implements WithCategory, OptionalCategoryAxisConfig, WithName, WithSeries {
    private chart: XYChart;
    private axis?: CategoryAxis;

    constructor(chart: XYChart) {
        this.chart = chart;
    }

    withHeight(_percent: number): OptionalCategoryAxisConfig {
        this.axis!.height = percent(_percent);
        return this;
    }

    applySeries(series: ColumnSeries): OptionalCategoryAxisConfig {
        series.yAxis = this.axis!;
        this.chart.series.push(series);
        return this;
    }

    getAxis(): CategoryAxis {
        return this.axis!;
    }

    inverseAxis(): OptionalCategoryAxisConfig {
        this.axis!.renderer.inversed = true;
        return this;
    }

    placeSeriesWithinGridLines(): OptionalCategoryAxisConfig {
        this.axis!.renderer.grid.template.location = 0;
        return this;
    }

    withMarginTop(marginTop: number): OptionalCategoryAxisConfig {
        this.axis!.marginTop = marginTop;
        return this;
    }

    withCategory(categoryName: string): WithSeries {
        this.axis!.dataFields.category = categoryName;
        return this;
    }

    newCategoryAxis(label: string): WithCategory {
        const axis = new CategoryAxis();

        axis.title.text = label;

        axis.title.fontWeight = "bold";

        this.axis = this.chart.yAxes.push(axis);

        return this;
    }
}

interface WithName {
    newCategoryAxis(label: string): WithCategory
}

interface WithCategory {
    withCategory(categoryName: string): WithSeries
}
interface WithSeries {
    applySeries(series: ColumnSeries): OptionalCategoryAxisConfig
}

interface OptionalCategoryAxisConfig {
    getAxis(): CategoryAxis;
    withHeight(percent: number): OptionalCategoryAxisConfig;
    placeSeriesWithinGridLines(): OptionalCategoryAxisConfig;
    inverseAxis(): OptionalCategoryAxisConfig;
    withMarginTop(marginTop: number): OptionalCategoryAxisConfig;
    applySeries(series: ColumnSeries): OptionalCategoryAxisConfig
}

export default categoryAxisBuilder;