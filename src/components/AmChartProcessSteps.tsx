import React from 'react';
import styled from 'styled-components';
import { Row, Col, Button } from "react-bootstrap";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import ChartBuilder from './ChartBuilder';
import { createYAxisBuilder as yAxisBuilder } from './YAxisBuilder';
import { createXAxisBuilder as xAxisBuilder } from './XAxisBuilder';
import { createLineSeriesBuilder as lineSeriesBuilder } from './LineSeriesBuilder';
import { columnSeriesBuilder } from './ColumnSeriesBuilder';
import Recipe from '../data/ladle.furnace';
import categoryAxisBuilder from './CategoryAxisBuilder';
import { DateAxis } from '@amcharts/amcharts4/charts';

am4core.useTheme(am4themes_animated);

class AmChartProcessSteps extends React.Component {
  private mainChart?: am4charts.XYChart;
  private processStepChart?: am4charts.XYChart;
  private recipe: Recipe = new Recipe();

  componentDidMount() {
    var container = am4core.create("chartdiv", am4core.Container);
    container.width = am4core.percent(100);
    container.height = am4core.percent(100);
    container.layout = "vertical";

    let chart = container.createChild(am4charts.XYChart);

    const chartBuilder = new ChartBuilder(chart);

    
    this.mainChart = chartBuilder
      .withLegend("top")
      .withXYCursor("zoomXY")
      .withData(this.recipe.getData())
      .withXZoombar()
      .withYZoombar()
      .build();

    const xAxisMainChart = xAxisBuilder(chart)
        .newDateAxis()
        .withTitle("Zeit")
        .withInterval({
            "timeUnit": "second",
            "count": 1
        })
        .withReferenceLine(new Date(Date.now() + 60000), "#000")
        .getAxis() as DateAxis;

    // xAxisMainChart.height = 0;
    xAxisMainChart.title.hide();
    xAxisMainChart.height = 0;
    xAxisMainChart.renderer.labels.template.hide();
    
    xAxisMainChart.strictMinMax = true;
    xAxisMainChart.min = new Date(Date.now()).getTime();
    xAxisMainChart.max = new Date(Date.now() + (60 * 60 * 1000)).getTime();
    // xAxisMainChart.hide();

    const power = yAxisBuilder(chart)  
        .newValueAxis("Wirkleistung [[MW]]", 0, 12)
        // .applyLineSeries(
        //   lineSeriesBuilder()
        //     .withName("Wirkleistung Soll [[MW]]")
        //     .withColor("#FF9914", 0.7)
        //     .withValueXProperty("time")
        //     .withValueYProperty("power")
        //     .withTooltipText("{valueY.formatNumber('#.00')} MW")
        //     .useSeriesColorForTooltip()
        //     .asThickLine()
        //     .asDashedLine()
        //     .getSeries()
        // )
        // .doNotSetAxisColor()
        .applyLineSeries(
            lineSeriesBuilder()
                .withName("Wirkleistung Ist [[MW]]")
                .withColor("#FF9914")
                .withValueXProperty("time")
                .withValueYProperty("powerAct")
                .withTooltipText("{valueY.formatNumber('#.00')} MW")
                .useSeriesColorForTooltip()
                .asThickLine()
                .asDashedLine()
                .getSeries()
        )
        .applySeriesColorToAxis()
        .onOpposite()
        .withNumberFormat("#.00")
        .getAxis();

    const temperature = yAxisBuilder(chart)
      .newValueAxis("Temperatur [[°C]]", 1400, 1660)
      .applyLineSeries(
          lineSeriesBuilder()
            .withName("Temperatur Soll [[°C]]")
            .withColor("#DC493A", 0.7)
            .withValueXProperty("time")
            .withValueYProperty("temperature")
            .withTooltipText("{valueY.formatNumber('###')} °C")
            .useSeriesColorForTooltip()
            .asThickLine()
            .getSeries()
      )
      .doNotSetAxisColor()
      .applyLineSeries(
        lineSeriesBuilder()
          .withName("Temperatur Ist [[°C]]")
          .withColor("#DC493A")
          .withValueXProperty("time")
          .withValueYProperty("temperatureAct")
          .withTooltipText("{valueY.formatNumber('###')} °C")
          .useSeriesColorForTooltip()
          .asThickLine()
          .getSeries()
      )
      .applySeriesColorToAxis()
      .applyLineSeries(
        lineSeriesBuilder()
          .withName("Materialzugabe Soll [[kg]]")
          .withColor("#584D3D", 0.7)
          .withValueXProperty("time")
          .withValueYProperty("y")
          .asEventSeries(am4core.Rectangle)
          .withEventTooltipText("{materialevent}")
          .showAlways()
          .finishEventSeriesConfiguration()
          .useSeriesColorForTooltip()
          .getSeries()
      )
      .doNotSetAxisColor()
      .applyLineSeries(
        lineSeriesBuilder()
          .withName("Materialzugabe Ist [[kg]]")
          .withColor("#584D3D")
          .withValueXProperty("time")
          .withValueYProperty("yAct")
          .asEventSeries(am4core.Rectangle)
          .withEventTooltipText("{materialeventAct}")
          .showAlways()
          .finishEventSeriesConfiguration()
          .useSeriesColorForTooltip()
          .getSeries()
      )
      .doNotSetAxisColor()
      .applyLineSeries(
        lineSeriesBuilder()
          .withName("Celox Messung")
          .withColor("#72A276")
          .withValueXProperty("time")
          .withValueYProperty("yAct")
          .asEventSeries(am4core.Circle)
          .withEventTooltipText("{materialeventAct}")
          .showAlways()
          .finishEventSeriesConfiguration()
          .useSeriesColorForTooltip()
          .getSeries()
      )
      .doNotSetAxisColor()
      .applyLineSeries(
        lineSeriesBuilder()
          .withName("Temperaturmessung Ist")
          .withColor("#DC493A")
          .withValueXProperty("time")
          .withValueYProperty("yTempAct")
          .asEventSeriesWithImage("/temperature.svg")
          .withEventTooltipText("{temperatureMeasurementInfoAct}")
          .showAlways()
          .finishEventSeriesConfiguration()
          .useSeriesColorForTooltip()
          .getSeries()
      )
      .doNotSetAxisColor()
      .getAxis();

    // Add keydown and keyup behavior to change the cursor behavior between zoomXY and panXY as it is very annoying not being able to pan the chart after zoom.
    document.addEventListener("keydown", this.handleStrgKeyDown, false);
    document.addEventListener("keyup", this.handleStrgKeyUp, false);

    let processStepChart = container.createChild(am4charts.XYChart);
    const steps = this.recipe.getProcessSteps();

    const processStepChartBuilder = new ChartBuilder(processStepChart);
    this.processStepChart = processStepChartBuilder
        .withData(steps)
        .withXYCursor("selectXY")
        .build();

    const xAxisProcessStepChart = xAxisBuilder(processStepChart)
        .newDateAxis()
        .withTitle("Zeit")
        .withInterval({
            "timeUnit": "second",
            "count": 1
        })
        .getAxis();

    xAxisProcessStepChart.strictMinMax = true;
    xAxisProcessStepChart.min = xAxisMainChart.min;
    xAxisProcessStepChart.max = xAxisMainChart.max;

    const processSeries = columnSeriesBuilder()
        .withName("Process Steps")
        .withColorProperty("color")
        .withDateXFromToProperty("from", "to")
        .withCategoryYProperty("name")
        .useSeriesColorForTooltip()
        .withHeightInPercent(80)
        .withTooltipText("Geplanter Start: {from}, Geplantes Ende: {to}")
        .withDateFormat("HH:mm")
        .getSeries();

    const processSeriesAct = columnSeriesBuilder()
        .withName("Process Steps")
        .withColorProperty("colorAct")
        .withDateXFromToProperty("fromAct", "toAct")
        .withCategoryYProperty("name")
        .useSeriesColorForTooltip()
        .withTooltipText("Prozessstart: {fromAct}")
        .withDateFormat("HH:mm:ss")
        .getSeries();

    categoryAxisBuilder(processStepChart)
        .newCategoryAxis("Prozessschritte")
        .withCategory("name")
        .applySeries(processSeries)
        .applySeries(processSeriesAct)
        .withHeight(80)
        .inverseAxis()
        .placeSeriesWithinGridLines()
        .getAxis();

    this.processStepChart.leftAxesContainer.width = 200;
    this.processStepChart.height = 300;
    this.mainChart.leftAxesContainer.width = this.processStepChart.leftAxesContainer.width;
    this.mainChart.rightAxesContainer.width = 200;
    this.processStepChart.rightAxesContainer.width = this.mainChart.rightAxesContainer.width;
  }

  private handleStrgKeyUp = (event: any) => {
    if(event.keyCode === 17) {
      this.mainChart!.cursor.behavior = "zoomXY";
    }
  }

  private handleStrgKeyDown = (event: any) => {
    if(event.keyCode === 17){
      this.mainChart!.cursor.behavior = "panXY";
    }
  }

  componentWillUnmount() {
    if (this.mainChart) {
      this.recipe.stopTrend();
      this.recipe.stopProcess();
      this.mainChart.dispose();
    }
  }

  startProcess = (processName: string) => {
    this.recipe!.beginProcess(processName, (data: any) => {
        this.processStepChart?.data.pop();
        this.processStepChart!.addData(data);
    });
  }

  startTrend = () => {
    this.recipe!.beginTrend((data: any) => {
      for(var i = 0; i< this.mainChart!.series.length; i++) {
        if(this.mainChart!.series.getIndex(i)?.name === "Temperatur Ist [[°C]]"){
          this.mainChart!.series.getIndex(i)?.addData(data);
        }
      }
    },
    (data: any) => {
      for(var i = 0; i< this.mainChart!.series.length; i++) {
        if(this.mainChart!.series.getIndex(i)?.name === "Wirkleistung Ist [[MW]]") {
          this.mainChart!.series.getIndex(i)?.addData(data);
        }
      }
    },
    (data: any) => {
      for(var i = 0; i< this.mainChart!.series.length; i++) {
        if(this.mainChart!.series.getIndex(i)?.name === "Materialzugabe Ist [[kg]]") {
          this.mainChart!.series.getIndex(i)?.addData(data);
        }
      }
    },
    (data: any) => {
      for(var i = 0; i< this.mainChart!.series.length; i++) {
        if(this.mainChart!.series.getIndex(i)?.name === "Temperaturmessung Ist") {
          this.mainChart!.series.getIndex(i)?.addData(data);
        }
      }
    });
  }

  mergeData(chartData: any[], data: any, beforeFirstPointCallback: any, indexCallback: any, afterLastPointCallback: any) {
    if(data.time.getTime() < chartData[0].time.getTime()){
      beforeFirstPointCallback();
    } else if(data.time.getTime() > chartData[chartData.length-1].time.getTime()){
      afterLastPointCallback();
    } else{
      for(var i = 0; i < chartData.length; i++) {
        if(i < chartData.length - 1) {
          const a = chartData[i].time.getTime();
          const b = chartData[i + 1].time.getTime();
          if(data.time.getTime() >= a && data.time.getTime() < b){
            indexCallback(i);
            break;
          }
        }
      }
    }
  }


  stopTrend = () => {
    this.recipe!.stopTrend();

    const axis = this.processStepChart!.xAxes.getIndex(0) as DateAxis;
    const mainAxis = this.mainChart!.xAxes.getIndex(0) as DateAxis;

    mainAxis.min = new Date(Date.now() - (60 * 60 * 1000)).getTime();

    axis.min = mainAxis.min;
    // axis.invalidate(); // not possible at once.
    setTimeout(() => {
      axis.min = mainAxis.min;
      axis.max = mainAxis.max;
      console.log("nothing happened?");
    }, 100);
  }

  render() {
    return <div>
      <Row className="mb-2">
      <Col>
        <h3>Processes</h3>
        <CustomButton onClick={() => this.startProcess("Heizen")}>Heizen</CustomButton>
        <CustomButton onClick={() => this.startProcess("Legieren")}>Legieren</CustomButton>
        <CustomButton onClick={() => this.startProcess("Homogenisieren")}>Homogenisieren</CustomButton>
      </Col>
    </Row>
    <Row>
      <Col>
        <h3>Trend</h3>
        <CustomButton onClick={() => this.startTrend()}>Start Trend</CustomButton>
        <CustomButton onClick={() => this.stopTrend()}>Stop Trend</CustomButton>
      </Col>
    </Row>

    <Row>
      <Col>
        <Graph id="chartdiv"></Graph>
      </Col>
    </Row>
    </div>
  }
}

const CustomButton = styled.button`
  background: rgba(215, 129, 106, 0.2);
  border-color: rgba(215, 129, 106, 0.2);
  border-radius: 0.25rem;
  padding: 0.375rem 0.75rem;
  -webkit-appearance: button;
  outline: none;
  transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  &:hover {
    background: rgba(215, 129, 106, 0.3);
    border-color: rgba(215, 129, 106, 0.3);
  }
`;

const Graph = styled.div`
  height: 800px;
`
const ProcessSteps = styled.div`
  height: 200px;
`

export default AmChartProcessSteps;