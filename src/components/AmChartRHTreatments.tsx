import React from 'react';
import styled from 'styled-components';
import { Row, Col, Button } from "react-bootstrap";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { anaCO, anaCO2, flowRate, absDecarb, decarbVel, signal, materialAdd } from "../data/fake.rhtreatment";
import ChartBuilder from './ChartBuilder';
import { createYAxisBuilder as yAxisBuilder } from './YAxisBuilder';
import { createXAxisBuilder as xAxisBuilder, createXAxisBuilderFromAxis } from './XAxisBuilder';
import { createLineSeriesBuilder as lineSeriesBuilder } from './LineSeriesBuilder';

am4core.useTheme(am4themes_animated);

class AmChartRHTreatments extends React.Component {

  private chartExhaustMeas?: am4charts.XYChart;
  private chartFlowRate?: am4charts.XYChart;
  private chartAbsDecarb?: am4charts.XYChart;
  private chartDecarbVel?: am4charts.XYChart;

  componentDidMount() {

    var container = am4core.create("chartdiv", am4core.Container);
    container.width = am4core.percent(100);
    container.height = am4core.percent(100);
    container.layout = "grid";

    let chartExhaust = container.createChild(am4charts.XYChart);
    let chartFlowRate = container.createChild(am4charts.XYChart);
    let chartAbsDecarb = container.createChild(am4charts.XYChart);
    let chartDecarbVel = container.createChild(am4charts.XYChart);

    const chartBuilderExhaustMeas = new ChartBuilder(chartExhaust);
    const chartBuilderFlowRate = new ChartBuilder(chartFlowRate);
    const chartBuilderAbsDecarb = new ChartBuilder(chartAbsDecarb);
    const chartBuilderDecarbVel = new ChartBuilder(chartDecarbVel);

    const measCOSeries = lineSeriesBuilder()
      .withName("Abgasanalyse CO [[%]]")
      .withColor("#DC493A")
      .withValueXProperty("time")
      .withValueYProperty("measCO")
      .withData(anaCO)
      .withTooltipText("{valueY.formatNumber('###')} %")
      .useSeriesColorForTooltip()
      .asThickLine()
      .getSeries();


    const measCO2Series = lineSeriesBuilder()
      .withName("Abgasanalyse CO" + "2" + "[[%]]")
      .withColor("#006400")
      .withValueXProperty("time")
      .withValueYProperty("measCO2")
      .withData(anaCO2)
      .withTooltipText("{valueY.formatNumber('###')} %")
      .useSeriesColorForTooltip()
      .asThickLine()
      .getSeries();


    const flowrateSeries = lineSeriesBuilder()
      .withName("Durchfluss Abgas [[kg/h]]")
      .withColor("#DC493A")
      .withValueXProperty("time")
      .withValueYProperty("flowrate")
      .withData(flowRate)
      .withTooltipText("{valueY.formatNumber('###')} kg/h")
      .useSeriesColorForTooltip()
      .asThickLine()
      .getSeries();

      const absDecarbSeries = lineSeriesBuilder()
      .withName("absolute Entkohlung [[kg/Messzyklus]]")
      .withColor("#DC493A")
      .withValueXProperty("time")
      .withValueYProperty("absDecarb")
      .withData(absDecarb)
      .withTooltipText("{valueY.formatNumber('###')} kg/Messzyklus")
      .useSeriesColorForTooltip()
      .asThickLine()
      .getSeries();

      const decarbVelSeries = lineSeriesBuilder()
      .withName("gleitende Entkohlung [[ppm/min]]")
      .withColor("#DC493A")
      .withValueXProperty("time")
      .withValueYProperty("decarbVel")
      .withData(decarbVel)
      .withTooltipText("{valueY.formatNumber('###')} ppm/min")
      .useSeriesColorForTooltip()
      .asThickLine()
      .getSeries();


      const signalSeries = lineSeriesBuilder()
      .withName("Signal Ende Entkohlung")
      .withColor("#0000Cd")
      .withValueXProperty("time")
      .withValueYProperty("signal")
      .withData(signal)
      .withTooltipText("{valueY.formatNumber('###')} Signal Entkohlung zuende")
      .useSeriesColorForTooltip()
      .asThickLine()
      .getSeries();


      const materialAdditionSeries = lineSeriesBuilder()
      .withName("Materialzugabe Soll [[kg]]")
      .withColor("#584D3D")
      .withValueXProperty("time")
      .withValueYProperty("y")
      .withData(materialAdd)
      .asEventSeries(am4core.Rectangle)
      .withEventTooltipText("{materialevent}: {value} kg")
      .showAlways()
      .finishEventSeriesConfiguration()
      .useSeriesColorForTooltip()
      .getSeries()

      //materialAdditionSeries.tooltipText = "{materialevent}: {value} kg";
      //materialAdditionSeries.showTooltipOn = "always";



    // exhaust measurement chart
    this.chartExhaustMeas = chartBuilderExhaustMeas
      .withLegend("top")
      .withXYCursor("zoomXY")
      .withXZoombar()
      .withYZoombar()
      .build();


    const xAxisExhaust = xAxisBuilder(chartExhaust)
      .newDateAxis()
      .withTitle("Zeit")
      .withInterval({
        "timeUnit": "second",
        "count": 1
      })
      .getAxis();

    const measExhaust = yAxisBuilder(chartExhaust)
      .newValueAxis("", 0, 80)
      .applyLineSeries(measCOSeries)
      .doNotSetAxisColor()
      .applyLineSeries(measCO2Series)
      .doNotSetAxisColor()
      .getAxis();



    // flowrate measurement chart
    this.chartFlowRate = chartBuilderFlowRate
      .withLegend("top")
      .withXYCursor("zoomXY")
      .withXZoombar()
      .withYZoombar()
      .build();


    const xAxisFlowRate = xAxisBuilder(chartFlowRate)
      .newDateAxis()
      .withTitle("Zeit")
      .withInterval({
        "timeUnit": "second",
        "count": 1
      })
      .getAxis();

    const measFlowRate = yAxisBuilder(chartFlowRate)
      .newValueAxis("", 0, 6000)
      .applyLineSeries(flowrateSeries)
      .doNotSetAxisColor()
      .getAxis();



     // abs. decarborisation measurement chart
     this.chartAbsDecarb = chartBuilderAbsDecarb
        .withLegend("top")
        .withXYCursor("zoomXY")
        .withXZoombar()
        .withYZoombar()
        .build();
  
  
      const xAxisAbsDecarb = xAxisBuilder(chartAbsDecarb)
        .newDateAxis()
        .withTitle("Zeit")
        .withInterval({
          "timeUnit": "second",
          "count": 1
        })
        .getAxis();
  
      const measAbsDecarb = yAxisBuilder(chartAbsDecarb)
        .newValueAxis("", 0, 5)
        .applyLineSeries(absDecarbSeries)
        .doNotSetAxisColor()
        .getAxis();


      // velocity of decarborisation measurement chart
     this.chartDecarbVel = chartBuilderDecarbVel
     .withLegend("top")
     .withXYCursor("zoomXY")
     .withXZoombar()
     .withYZoombar()
     .build();


   const xAxisDecarbVel = xAxisBuilder(chartDecarbVel)
     .newDateAxis()
     .withTitle("Zeit")
     .withInterval({
       "timeUnit": "second",
       "count": 1
     })
     .getAxis();

   const measDecarbVel = yAxisBuilder(chartDecarbVel)
     .newValueAxis("", -10, 0)
     .applyLineSeries(decarbVelSeries)
     .doNotSetAxisColor()
     .applyLineSeries(signalSeries)
     .doNotSetAxisColor()
     .applyLineSeries(materialAdditionSeries)
     .doNotSetAxisColor()
     .getAxis();



    this.chartExhaustMeas.width = am4core.percent(50);
    this.chartFlowRate.width = am4core.percent(50);
    this.chartAbsDecarb.width = am4core.percent(50);
    this.chartDecarbVel.width = am4core.percent(50);
    this.chartExhaustMeas.height = am4core.percent(50);
    this.chartFlowRate.height = am4core.percent(50);
    this.chartAbsDecarb.height = am4core.percent(50);
    this.chartDecarbVel.height = am4core.percent(50);




    // Add keydown and keyup behavior to change the cursor behavior between zoomXY and panXY as it is very annoying not being able to pan the chart after zoom.
    document.addEventListener("keydown", this.handleStrgKeyDown, false);
    document.addEventListener("keyup", this.handleStrgKeyUp, false);

  }

  private handleStrgKeyUp = (event: any) => {
    if(event.keyCode === 17) {
      this.chartExhaustMeas!.cursor.behavior = "zoomXY";
    }
  }

  private handleStrgKeyDown = (event: any) => {
    if(event.keyCode === 17){
      this.chartExhaustMeas!.cursor.behavior = "panXY";
    }
  }

  componentWillUnmount() {
    if (this.chartExhaustMeas) {
      this.chartExhaustMeas.dispose();
    }
  }

  render() {
    return <div>
    <Row>
      <Col>
        <Graph id="chartdiv"></Graph>
      </Col>
    </Row>
    </div>
  }
}

const CustomButton = styled.button`
  background: rgba(215, 129, 106, 0.2);
  border-color: rgba(215, 129, 106, 0.2);
  border-radius: 0.25rem;
  padding: 0.375rem 0.75rem;
  -webkit-appearance: button;
  outline: none;
  transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  &:hover {
    background: rgba(215, 129, 106, 0.3);
    border-color: rgba(215, 129, 106, 0.3);
  }
`;

const Graph = styled.div`
  height: 800px;
`

export default AmChartRHTreatments;