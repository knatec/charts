import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import { color, ITimeInterval } from "@amcharts/amcharts4/core";

function createXAxisBuilderFromAxis(axis: am4charts.DateAxis): Build {
    const builder = new XAxisBuilder(axis.chart);
    builder.axis = axis;
    return builder;
}

function xAxisBuilder(chart: am4charts.XYChart): WithDateAxis {
    const builder = new XAxisBuilder(chart);
    return builder;
}

class XAxisBuilder implements WithDateAxis, WithTitle, WithInterval, Build {
    private chart: am4charts.XYChart;
    axis?: am4charts.DateAxis;

    constructor(chart: am4charts.XYChart) {
        this.chart = chart;
    }

    withReferenceLine(target: Date, colorHex: string): Build {
        const range = this.axis!.axisRanges.create();
        range.date = target;
        range.grid.stroke = color(colorHex);
        range.grid.strokeWidth = 2;
        range.grid.strokeOpacity = 1;
        return this;
    }

    withTitle(title: string): WithInterval {
        this.axis!.title.text = title;
        return this;
    }

    withRanges(processRanges: Range[]): Build {
        processRanges.forEach(r => {
          const range = this.axis!.axisRanges.create();
          range.date = r.start;
          range.endDate = r.end;
          range.axisFill.fill = am4core.color("#D7816A");
          range.axisFill.fillOpacity = 0.2;
          
          range.label.text = "Process";
          range.label.inside = true;
          range.label.rotation = 90;
          range.label.horizontalCenter = "right";
          range.label.verticalCenter = "bottom";
        });
        return this;
    }

    withZoomChangedEventHandler(eventHandler: any): Build {
        this.axis!.events.on("startendchanged", (ev) => {
            eventHandler(ev);
        });
        return this;
    }
    
    withInterval(interval: am4core.ITimeInterval): Build {
        this.axis!.baseInterval = interval;
        return this;
    }

    public newDateAxis(): WithTitle {
        const dateAxis = new am4charts.DateAxis();
        this.axis = this.chart.xAxes.push(dateAxis);
        return this;
    }

    public getAxis(): am4charts.DateAxis {
        return this.axis!;
    }
}


interface Build {
    withReferenceLine(target: Date, colorHex: string): Build;
    withZoomChangedEventHandler(eventHandler: any): Build
    withRanges(ranges: Range[]): Build
    getAxis(): am4charts.DateAxis
}
interface WithInterval {
    withInterval(interval: ITimeInterval): Build
}

interface WithTitle {
    withTitle(title: string) : WithInterval
}    

interface WithDateAxis {
    newDateAxis(): WithTitle
}

export interface Range {
    start: Date,
    end: Date
}

export { xAxisBuilder as createXAxisBuilder, createXAxisBuilderFromAxis };
