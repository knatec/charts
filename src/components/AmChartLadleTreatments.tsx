import React from 'react';
import styled from 'styled-components';
import { Row, Col, Button } from "react-bootstrap";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import ChartBuilder from './ChartBuilder';
import { createYAxisBuilder as yAxisBuilder } from './YAxisBuilder';
import { createXAxisBuilder as xAxisBuilder } from './XAxisBuilder';
import { createLineSeriesBuilder as lineSeriesBuilder } from './LineSeriesBuilder';
import { columnSeriesBuilder } from './ColumnSeriesBuilder';
import categoryAxisBuilder from './CategoryAxisBuilder';
import { temperatures, powers, steps, temperatureMeasurements } from '../data/ladle.treatment';

am4core.useTheme(am4themes_animated);

class AmChartLadleTreatments extends React.Component {
  private mainChart?: am4charts.XYChart;
  private processStepChart?: am4charts.XYChart;

  componentDidMount() {
    var container = am4core.create("chartdiv", am4core.Container);
    container.width = am4core.percent(100);
    container.height = am4core.percent(100);
    container.layout = "vertical";

    let chart = container.createChild(am4charts.XYChart);

    const chartBuilder = new ChartBuilder(chart);

    
    this.mainChart = chartBuilder
      .withLegend("top")
      .withXYCursor("zoomXY")
      .withXZoombar()
      .withYZoombar()
      .build();

    const xAxis = xAxisBuilder(chart)
        .newDateAxis()
        .withTitle("Zeit")
        .withInterval({
            "timeUnit": "second",
            "count": 1
        })
        .getAxis();

    const power = yAxisBuilder(chart)  
        .newValueAxis("Power [[MW]]", 0, 12)
        .applyLineSeries(
            lineSeriesBuilder()
                .withName("Power [[MW]]")
                .withColor("#04724D")
                .withValueXProperty("time")
                .withValueYProperty("power")
                .withTooltipText("{valueY.formatNumber('#.00')} MW")
                .useSeriesColorForTooltip()
                .asThickLine()
                .withData(powers)
                .getSeries()
        )
        .applySeriesColorToAxis()
        .onOpposite()
        .withNumberFormat("#.00")
        .getAxis();

    const temperature = yAxisBuilder(chart)
      .newValueAxis("Temperature Curve [[°C]]", 0, 1800)
      .applyLineSeries(
        lineSeriesBuilder()
          .withName("Temperature [[°C]]")
          .withColor("#DC493A")
          .withValueXProperty("time")
          .withValueYProperty("temperature")
          .withTooltipText("{valueY.formatNumber('###')} °C")
          .useSeriesColorForTooltip()
          .asThickLine()
          .withData(temperatures)
          .getSeries()
      )
      .applySeriesColorToAxis()
      .applyColumnSeries(
        columnSeriesBuilder()
          .withName("Temperature Measurement [[°C]]")
          .withColor("#336699")
          .withDateXProperty("time")
          .withValueYProperty("temperatureMeasurement")
          .withData(temperatureMeasurements)
          .withBulletOnTop()
          .getSeries()
      )
      .doNotSetAxisColor()
      .getAxis();

    // Add keydown and keyup behavior to change the cursor behavior between zoomXY and panXY as it is very annoying not being able to pan the chart after zoom.
    document.addEventListener("keydown", this.handleStrgKeyDown, false);
    document.addEventListener("keyup", this.handleStrgKeyUp, false);


    let processStepChart = container.createChild(am4charts.XYChart);

    const processStepChartBuilder = new ChartBuilder(processStepChart);
    this.processStepChart = processStepChartBuilder
        .withData(steps)
        .withXYCursor("zoomXY")
        .build();
        
    processStepChart.xAxes.push(xAxis);
    
    const processSeriesAct = columnSeriesBuilder()
        .withName("Process Steps")
        .withColorProperty("color")
        .withDateXFromToProperty("from", "to")
        .withCategoryYProperty("name")
        .useSeriesColorForTooltip()
        .withTooltipText("Start: {from}")
        .withDateFormat("HH:mm:ss")
        .withLabelProperty("label")
        .withHeightInPercent(40)
        .getSeries();

    categoryAxisBuilder(processStepChart)
        .newCategoryAxis("Position")
        .withCategory("name")
        .applySeries(processSeriesAct)
        .inverseAxis()
        .getAxis();

    this.processStepChart.leftAxesContainer.width = 200;
    this.mainChart.leftAxesContainer.width = this.processStepChart.leftAxesContainer.width;
    this.mainChart.rightAxesContainer.width = 200;
    this.processStepChart.rightAxesContainer.width = this.mainChart.rightAxesContainer.width;
  }

  private handleStrgKeyUp = (event: any) => {
    if(event.keyCode === 17) {
      this.mainChart!.cursor.behavior = "zoomXY";
    }
  }

  private handleStrgKeyDown = (event: any) => {
    if(event.keyCode === 17){
      this.mainChart!.cursor.behavior = "panXY";
    }
  }

  componentWillUnmount() {
    if (this.mainChart) {
      this.mainChart.dispose();
    }
  }

  render() {
    return <div>
    <Row>
      <Col>
        <Graph id="chartdiv"></Graph>
      </Col>
    </Row>
    </div>
  }
}

const CustomButton = styled.button`
  background: rgba(215, 129, 106, 0.2);
  border-color: rgba(215, 129, 106, 0.2);
  border-radius: 0.25rem;
  padding: 0.375rem 0.75rem;
  -webkit-appearance: button;
  outline: none;
  transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  &:hover {
    background: rgba(215, 129, 106, 0.3);
    border-color: rgba(215, 129, 106, 0.3);
  }
`;

const Graph = styled.div`
  height: 800px;
`
const ProcessSteps = styled.div`
  height: 400px;
`

export default AmChartLadleTreatments;