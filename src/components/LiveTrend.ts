import { XYChart } from "@amcharts/amcharts4/charts";
import { startLiveSinus } from "../data/fake.data";

class LiveTrend {
    private chart: XYChart;
    private liveDataStart?: Date;
    private count: number = 0;
    private seriesInterval: number;
    private liveDataInterval: number;
    private lastTimestamp: number;
    private intervalId?: number;

    constructor (chart: XYChart, seriesInterval: number, liveDataInterval: number) {
        this.chart = chart;
        this.seriesInterval = seriesInterval;
        this.liveDataInterval = liveDataInterval;
        this.lastTimestamp = this.chart.data[this.chart.data.length - 1].time;
    }

    public start() {
        if(this.intervalId)
            return;

        console.info("starting");
        this.intervalId = startLiveSinus((data:any, interval: number) => {
            if(!this.liveDataStart) {
                this.liveDataStart = new Date();
            }

            this.count += this.liveDataInterval;

            var removeCount = 0;
            if(this.hasToRemovePointsBeforeLiveStart()) {
                removeCount = this.count > this.seriesInterval ? 1 : 0;
                this.lastTimestamp = this.chart.data[0].time;
            }

            this.chart?.addData(data, removeCount);
            if(removeCount > 0)
                this.count = 0;
        })
    };

    public stop() {
        clearInterval(this.intervalId);
        this.intervalId = undefined;
    }

    private hasToRemovePointsBeforeLiveStart() {
        return this.lastTimestamp! < this.liveDataStart!.getTime();
    }
}

export default LiveTrend;