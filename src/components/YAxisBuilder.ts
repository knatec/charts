import * as am4charts from "@amcharts/amcharts4/charts";
import { color, NumberFormatter } from "@amcharts/amcharts4/core";

function valueAxisBuilder(chart: am4charts.XYChart): NewAxis {
    return new LineSeriesAxisBuilder(chart);
}

function createYAxisBuilderFromAxis(axis: am4charts.ValueAxis): OptionalLineSeriesConfig {
    const builder = new LineSeriesAxisBuilder(axis.chart);
    builder.axis = axis;
    return builder;
}

class LineSeriesAxisBuilder implements NewAxis, ApplyLineSeries, ApplySeriesColor, OptionalLineSeriesConfig {
    private chart: am4charts.XYChart;
    public axis?: am4charts.ValueAxis;
    public categoryAxis?: am4charts.ValueAxis;

    constructor(chart: am4charts.XYChart) {
        this.chart = chart;
    }

    doNotSetAxisColor(): OptionalLineSeriesConfig {
        return this;
    }

    public newValueAxis(label: string, min?: number, max?: number): ApplyLineSeries {
        const axis = new am4charts.ValueAxis();
        axis.title.text = label;
        axis.title.marginLeft = 20;
        
        axis.min = min ?? undefined;
        axis.max = max ?? undefined;

        axis.title.fontWeight = "bold";

        this.axis = this.chart.yAxes.push(axis);

        return this;
    }

    public onOpposite(): OptionalLineSeriesConfig {
        this.axis!.renderer.opposite = true;
        this.axis!.title.marginLeft = 0;
        this.axis!.title.marginRight = 20;
        return this;
    }

    public asLogarithmic(): OptionalLineSeriesConfig {
        this.axis!.logarithmic = true;
        return this;
    }
  
    public applyLineSeries(series: am4charts.LineSeries): ApplySeriesColor {
        series.yAxis = this.axis!;
        this.chart.series.push(series);
        return this;
    }

    public applyColumnSeries(series: am4charts.ColumnSeries): ApplySeriesColor {
        series.yAxis = this.axis!;
        this.chart.series.push(series);
        return this;
    }

    public withChangingLineColor(lowerLimit: number, colorBelow: string, upperLimit: number, colorAbove: string): OptionalLineSeriesConfig {
        if(this.axis!.series.length > 0) {
            const lastAddedSeries = this.axis!.series.getIndex(this.axis!.series.length - 1) as am4charts.XYSeries;
            var range = this.axis!.createSeriesRange(lastAddedSeries);
            range.value = lowerLimit
            range.endValue = -10000;
            range.contents.stroke = color(colorBelow);
            range.contents.strokeWidth = 3;

            var rangeUpper = this.axis!.createSeriesRange(lastAddedSeries);
            rangeUpper.value = upperLimit
            rangeUpper.endValue = 10000;
            rangeUpper.contents.stroke = color(colorAbove);
            rangeUpper.contents.strokeWidth = 3;
        }
        return this;
    }

    public withReferenceLine(yValue: number, colorHexCode: string): OptionalLineSeriesConfig {
        const range = this.axis!.axisRanges.create();
        range.value = yValue;
        range.grid.stroke = color(colorHexCode);
        range.grid.strokeWidth = 2;
        range.grid.strokeOpacity = 1;
        return this;
    }

    public applySeriesColorToAxis(): OptionalLineSeriesConfig {
        if(this.axis!.series.length > 0) {
            const lastAddedSeries = this.axis!.series.getIndex(this.axis!.series.length - 1) as am4charts.XYSeries;
            this.axis!.renderer.line.strokeOpacity = 1;
            this.axis!.renderer.line.strokeWidth = 2;
            this.axis!.renderer.line.stroke = lastAddedSeries.stroke;
            this.axis!.renderer.labels.template.fill = lastAddedSeries.stroke;
        }
        return this;
    }

    public withNumberFormat(format: string): OptionalLineSeriesConfig {
        this.axis!.numberFormatter = new NumberFormatter();
        this.axis!.numberFormatter.numberFormat = format;
        this.axis!.adjustLabelPrecision = false;
        return this;
    }

    public getAxis(): am4charts.ValueAxis {
        return this.axis!;
    }
}


interface NewAxis {
    newValueAxis(label: string, min?: number, max?: number): ApplyLineSeries;
}

interface ApplyLineSeries {
    applyLineSeries(series: am4charts.LineSeries): ApplySeriesColor;
}

interface ApplySeriesColor {
    applySeriesColorToAxis(): OptionalLineSeriesConfig;
    doNotSetAxisColor(): OptionalLineSeriesConfig;
}

interface OptionalLineSeriesConfig {
    getAxis(): am4charts.Axis;
    withNumberFormat(format: string): OptionalLineSeriesConfig;
    onOpposite(): OptionalLineSeriesConfig;
    asLogarithmic(): OptionalLineSeriesConfig;
    withChangingLineColor(lowerLimit: number, colorBelow: string, upperLimit: number, colorAbove: string): OptionalLineSeriesConfig;
    withReferenceLine(yValue: number, colorHexCode: string): OptionalLineSeriesConfig;

    applyLineSeries(series: am4charts.LineSeries): ApplySeriesColor;
    applyColumnSeries(series: am4charts.ColumnSeries): ApplySeriesColor;
}

export { valueAxisBuilder as createYAxisBuilder, createYAxisBuilderFromAxis }