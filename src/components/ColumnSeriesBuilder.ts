import { ColumnSeries, XYChart, Bullet } from "@amcharts/amcharts4/charts";
import { color, percent, Label, Optional, Circle } from "@amcharts/amcharts4/core";

const columnSeriesBuilder = (): WithName => {
    return new ColumnSeriesBuilder();
}


class ColumnSeriesBuilder implements WithName, WithValueX, WithValueY, WithColor, OptionalConfiguration {
    private series: ColumnSeries = new ColumnSeries();

    withName(name: string): WithColor {
        this.series.name = name;
        return this;
    }

    withHeightInPercent(_percent: number): OptionalConfiguration {
        this.series.columns.template.height = percent(_percent);
        return this;
    }

    withLabelProperty(labelPropertyName: string) : OptionalConfiguration {
        let label = this.series.columns.template.createChild(Label);
        label.stroke = color("#fff");
        label.toFront();
        label.fontWeight = "100";
        label.marginLeft = 10;
        label.align = "left";
        label.valign = "middle";
        label.hideOversized = false;
        label.truncate = true;

        label.adapter.add("text", function(text, target: any) {
            if (!target.dataItem) {
                return text;
            }

            return target.dataItem.dataContext[labelPropertyName];
        });
        return this;
    }

    withColorProperty(colorPropertyName: string): WithValueX {
        this.series.columns.template.propertyFields.fill = colorPropertyName;
        this.series.columns.template.propertyFields.stroke = colorPropertyName;
        return this;
    }

    withColor(colorHexCode: string): WithValueX {
        this.series.fill = color(colorHexCode);
        this.series.stroke = color(colorHexCode);
        return this;
    }



    withDateXFromToProperty(propertyFrom: string, propertyTo: string): WithValueY {
        this.series.dataFields.openDateX = propertyFrom;
        this.series.dataFields.dateX = propertyTo;
        return this;
    }

    withCategoryYProperty(propertyName: string): OptionalConfiguration {
        this.series.dataFields.categoryY = propertyName;
        return this;
    }

    withValueYProperty(propertyName: string): OptionalConfiguration {
        this.series.dataFields.valueY = propertyName;
        return this;
    }

    withDateXProperty(property: string): WithValueY {
        this.series.dataFields.dateX = property;
        return this;
    }

    useSeriesColorForTooltip(): OptionalConfiguration {
        return this;
    }

    withTooltipText(text: string): OptionalConfiguration {
        this.series.columns.template.tooltipText = text;
        return this;
    }

    withDateFormat(format: string): OptionalConfiguration {
        this.series.dateFormatter.dateFormat = format;
        return this;
    }

    withBulletOnTop(): OptionalConfiguration {
        var bullet = this.series.bullets.push(new Bullet());
        var circle = bullet.createChild(Circle);
        circle.horizontalCenter = "middle";
        circle.verticalCenter = "middle";
        circle.width = 5;
        circle.height = 5;
        return this;
    }

    withData(data: any[]): OptionalConfiguration {
        this.series.data = data;
        return this;
    }

    getSeries(): ColumnSeries {
        return this.series;
    }
}

interface WithName {
    withName(name: string): WithColor
}
interface WithColor {
    withColorProperty(colorPropertyName: string): WithValueX
    withColor(color: string): WithValueX
}

interface WithValueX {
    withDateXFromToProperty(propertyFrom: string, propertyTo: string): WithValueY
    withDateXProperty(property: string): WithValueY
}

interface WithValueY {
    withValueYProperty(propertyName: string): OptionalConfiguration
    withCategoryYProperty(propertyName: string): OptionalConfiguration
}

interface OptionalConfiguration {
    withHeightInPercent(percent: number): OptionalConfiguration;
    withDateFormat(format: string): OptionalConfiguration;
    withTooltipText(text: string): OptionalConfiguration;
    useSeriesColorForTooltip(): OptionalConfiguration;
    withLabelProperty(labelPropertyName: string): OptionalConfiguration;
    withBulletOnTop(): OptionalConfiguration;
    withData(data: any[]): OptionalConfiguration;
    getSeries(): ColumnSeries;
}

export { columnSeriesBuilder }
