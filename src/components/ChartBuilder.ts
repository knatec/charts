import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";

type ZoomType = "zoomX" | "zoomY" | "zoomXY" | "selectX" | "selectY" | "selectXY" | "panX" | "panY" | "panXY" | "none";

class ChartBuilder {
    private chart: am4charts.XYChart;

    constructor(chart: am4charts.XYChart) {
        this.chart = chart;
    }

    public getChart(): am4charts.XYChart {
        return this.chart;
    }
    
    public withLegend(legendPosition: am4charts.LegendPosition): ChartBuilder {
        const legend = new am4charts.Legend();
        legend.position = legendPosition;
        this.chart.legend = legend;
        return this;
    }

    public withData(data: any[]): ChartBuilder {
        this.chart.data = data;
        return this;
    }

    public withXYCursor(type: ZoomType): ChartBuilder {
        const cursor = new am4charts.XYCursor();
        cursor.behavior = type;
        this.chart.cursor = cursor;
        return this;
    }

    public withXZoombar(): ChartBuilder {
        this.chart.scrollbarX = new am4core.Scrollbar();  
        return this;
    }

    public withYZoombar(): ChartBuilder {
        this.chart.scrollbarY = new am4core.Scrollbar();  
        return this;
    }

    public build(): am4charts.XYChart{
        return this.chart;
    }

    public dispose() {
        this.chart.dispose();
    }
}

export default ChartBuilder;