import React from 'react';
import styled from 'styled-components';
import { Row, Col, Button } from "react-bootstrap";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { points, sinusPoints, processRanges, startLiveData, zoomData, startLiveSinus } from "../data/fake.data"
import ChartBuilder from './ChartBuilder';
import { createYAxisBuilder as yAxisBuilder } from './YAxisBuilder';
import { createXAxisBuilder as xAxisBuilder, createXAxisBuilderFromAxis } from './XAxisBuilder';
import { createLineSeriesBuilder as lineSeriesBuilder } from './LineSeriesBuilder';
import LiveTrend from './LiveTrend';

am4core.useTheme(am4themes_animated);

class AmChartLiveTrend extends React.Component {

  private chart?: am4charts.XYChart;
  private liveTrend?: LiveTrend;
  private loadDataTimer: any = undefined;

  componentDidMount() {
    let chart = am4core.create("chartdiv", am4charts.XYChart)
    const chartBuilder = new ChartBuilder(chart);

    console.info(sinusPoints);
    this.chart = chartBuilder
      .withLegend("top")
      .withXYCursor("zoomXY")
      .withData(sinusPoints)
      .withXZoombar()
      .withYZoombar()
      .build();


    const xAxis = xAxisBuilder(chart)
      .newDateAxis()
      .withTitle("Zeit")
      .withInterval({
        "timeUnit": "millisecond",
        "count": 100
      })
      .withZoomChangedEventHandler(this.dateAxisChanged)
      .getAxis();

    const sinusSeries = lineSeriesBuilder()
      .withName("Sinus")
      .withColor("#DC493A")
      .withValueXProperty("time")
      .withValueYProperty("sinus")
      .withTooltipText("{time.formatDate('HH:mm:ss:SSS')} - {valueY.formatNumber('###')} °C")
      .useSeriesColorForTooltip()
      .getSeries();

    const sinus = yAxisBuilder(chart)
      .newValueAxis("Sinus")
      .applyLineSeries(sinusSeries)
      .applySeriesColorToAxis()
      .getAxis();

    this.liveTrend = new LiveTrend(this.chart, 1000, 100);
    this.liveTrend.start();

    // Add keydown and keyup behavior to change the cursor behavior between zoomXY and panXY as it is very annoying not being able to pan the chart after zoom.
    document.addEventListener("keydown", this.handleStrgKeyDown, false);
    document.addEventListener("keyup", this.handleStrgKeyUp, false);
  }

  private dateAxisChanged = (ev: any) => {
    console.info("axis changed");
    if(this.loadDataTimer)
      clearTimeout(this.loadDataTimer);
      
    this.loadDataTimer = setTimeout(() => {
      let start = ev.target.minZoomed;
      let end = ev.target.maxZoomed;
 
      zoomData(new Date(start), new Date(end), 500);
      this.loadDataTimer = undefined;
    }, 500);
  }

  private handleStrgKeyUp = (event: any) => {
    if(event.keyCode === 17) {
      this.chart!.cursor.behavior = "zoomXY";
    }
  }

  private handleStrgKeyDown = (event: any) => {
    if(event.keyCode === 17){
      this.chart!.cursor.behavior = "panXY";
    }
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
      this.liveTrend?.stop();
    }
  }

  removeAxesRanges = () => {
    const dateAxis = this.chart!.xAxes.values[0];
    dateAxis.axisRanges.clear();
  }

  addAxesRanges = () => {
    const dateAxis = this.chart!.xAxes.values[0] as am4charts.DateAxis;
    
    createXAxisBuilderFromAxis(dateAxis)
      .withRanges(processRanges);
  }

  startLiveChart = () => {
    this.liveTrend?.start();
  }

  stopLiveChart = () => {
    this.liveTrend?.stop();
  }

  render() {
    return <div>
    <Row>
      <Col>
        <h3>Axes Ranges</h3>
        <CustomButton onClick={this.addAxesRanges}>Add</CustomButton>
        <CustomButton className="ml-2" onClick={this.removeAxesRanges}>Remove</CustomButton>
      </Col>
    </Row>
    <Row>
      <Col>
        <h3>Live Chart</h3>
        <CustomButton onClick={this.startLiveChart}>Start</CustomButton>
        <CustomButton className="ml-2" onClick={this.stopLiveChart}>Stop</CustomButton>
      </Col>
    </Row>
    <Row>
      <Col>
        <Graph id="chartdiv"></Graph>
      </Col>
    </Row>
    </div>
  }
}

const CustomButton = styled.button`
  background: rgba(215, 129, 106, 0.2);
  border-color: rgba(215, 129, 106, 0.2);
  border-radius: 0.25rem;
  padding: 0.375rem 0.75rem;
  -webkit-appearance: button;
  outline: none;
  transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  &:hover {
    background: rgba(215, 129, 106, 0.3);
    border-color: rgba(215, 129, 106, 0.3);
  }
`;

const Graph = styled.div`
  height: 800px;
`

export default AmChartLiveTrend;