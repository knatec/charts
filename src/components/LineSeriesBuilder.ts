import { LineSeries, Bullet } from "@amcharts/amcharts4/charts";
import { color, Rectangle, Optional, Sprite, Image, Label } from "@amcharts/amcharts4/core";

const lineSeriesBuilder = (): WithName => {
    return new LineSeriesBuilder();
}

class LineSeriesBuilder implements WithName, WithValueX, WithValueY, WithColor, OptionalConfiguration, EventSeriesConfig, EventSeriesTooltipConfig {
    private series: LineSeries = new LineSeries();
    private bullet?: Bullet;
    private label?: Label;

    withName(name: string): WithColor {
        this.series.name = name;
        return this;
    }

    withColor(colorHexCode: string, lighten?: number): WithValueX {
        this.series.stroke = color(colorHexCode);
        if(lighten) {
            this.series.stroke = this.series.stroke.lighten(lighten);
        }
        return this;
    }

    withValueYProperty(propertyName: string) {
        this.series.dataFields.valueY = propertyName;
        return this;
    }

    withValueXProperty(propertyName: string): WithValueY {
        this.series.dataFields.dateX = propertyName;
        return this;
    }

    useSeriesColorForTooltip(): OptionalConfiguration {
        this.series.tooltip!.getStrokeFromObject = true;
        this.series.tooltip!.getFillFromObject = false;
        this.series.tooltip!.background.fill = this.series.stroke;
        return this;
    }

    withTooltipText(text: string): OptionalConfiguration {
        this.series.tooltipText = text;
        return this;
    }

    withEventTooltipText(text: string): EventSeriesTooltipConfig {
        this.bullet!.tooltipText = text;
        return this;
    }

    withInnerLabelProperty(property: string, colorHexCode: string = "#fff"): EventSeriesConfig {
        const label = this.bullet!.createChild(Label);
        
        label.toFront();
        label.align = "left";
        label.fontWeight = "bold";

        label.valign = "middle";
        label.hideOversized = false;
        label.truncate = true;
        label.paddingLeft = -10;
        label.paddingTop = -10;

        label.fill = color(colorHexCode);
        
        label.adapter.add("text", function(text, target: any) {
            if (!target.dataItem) {
                 return text;
            }

            return target.dataItem.dataContext[property];
        });

        this.label = label;
        return this;
    }

    withChangingLabelColor(property: string, colorHexCode: string = "#fff"): EventSeriesConfig {
        if(this.label){
            const label = this.label!;
            label.adapter.add("fill", function(fill, target: any) {
                if (!target.dataItem) {
                    return fill;
            }
            
                if(target.dataItem.dataContext[property])
                    return color(colorHexCode);

                return fill;
            });
        } else {
            throw new Error("Please call withInnerLabelProperty first.");
        }

        return this;
    }
    
    showAlways(): EventSeriesConfig {
        this.bullet!.showTooltipOn = "always";
        return this;
    }

    finishEventSeriesConfiguration(): OptionalConfiguration {
        this.bullet = undefined;
        return this;
    }

    asEventSeries<T extends Sprite>(type: { new():T }): EventSeriesConfig {
        this.series.strokeOpacity = 0;
        this.series.tooltip!.pointerOrientation = "down";
        this.series.tooltip!.dy = -10;

        var bullet = this.series.bullets.push(new Bullet());
        
        var rectangle = bullet.createChild(type);
        rectangle.fill = this.series.stroke;
        rectangle.verticalCenter = "middle";
        rectangle.horizontalCenter = "middle";
        rectangle.width = 20;
        rectangle.height = 20;
        rectangle.strokeOpacity = 0.5;
        rectangle.stroke = color("#ffffff");
        rectangle.nonScalingStroke = true;
        
        this.bullet = bullet;
        return this;
    }

    asEventSeriesWithImage(href: string): EventSeriesConfig {
        this.series.strokeOpacity = 0;
        this.series.tooltip!.pointerOrientation = "down";
        this.series.tooltip!.dy = -10;

        var bullet = this.series.bullets.push(new Bullet());
        
        var rectangle = bullet.createChild(Image);
        rectangle.href = href;
        rectangle.verticalCenter = "middle";
        rectangle.horizontalCenter = "middle";
        rectangle.width = 30;
        rectangle.height = 30;
        rectangle.nonScalingStroke = true;
    
        this.bullet = bullet;
        return this;
    }


    asDottedLine(): OptionalConfiguration {
        this.series.strokeDasharray = "2,4";
        return this;
    }

    asWideDottedLine(): OptionalConfiguration {
        this.series.strokeDasharray = "2,8";
        return this;
    }

    asDashDottedLine(): OptionalConfiguration {
        this.series.strokeDasharray = "8,4,2,4";
        return this;
    }

    asDashedLine(): OptionalConfiguration {
        this.series.strokeDasharray = "8,4";
        return this;
    }

    asWideDashedLine(): OptionalConfiguration {
        this.series.strokeDasharray = "8,12";
        return this;
    }

    asThickLine(): OptionalConfiguration {
        this.series.strokeWidth = 3;
        return this;
    }

    asLightLine(): OptionalConfiguration {
        this.series.strokeOpacity = 0.3;
        return this;
    }

    withTension(x: number, y: number): OptionalConfiguration {
        this.series.tensionX = x;
        this.series.tensionY = y;
        return this;
    }

    withData(data: any[]): OptionalConfiguration {
        this.series.data = data;
        return this;
    }


    getSeries(): LineSeries {
        return this.series;
    }
}

interface WithName {
    withName(name: string): WithColor
}
interface WithColor {
    withColor(colorHexCode: string, lighten?: number): WithValueX
}

interface WithValueX {
    withValueXProperty(propertyName: string): WithValueY
}

interface WithValueY {
    withValueYProperty(propertyName: string): OptionalConfiguration
}

interface OptionalConfiguration {
    withData(temperatures: any[]): OptionalConfiguration;
    withTension(x: number, y: number): OptionalConfiguration;
    withTooltipText(text: string): OptionalConfiguration;
    useSeriesColorForTooltip(): OptionalConfiguration;
    asEventSeries<T extends Sprite>(type: { new():T }): EventSeriesConfig;
    asEventSeriesWithImage(href: string): EventSeriesConfig;
    asDottedLine(): OptionalConfiguration;
    asDashDottedLine(): OptionalConfiguration;
    asDashedLine(): OptionalConfiguration;
    asThickLine(): OptionalConfiguration;
    asWideDashedLine(): OptionalConfiguration;
    asLightLine(): OptionalConfiguration;
    asWideDottedLine(): OptionalConfiguration;
    getSeries(): LineSeries;
}

interface EventSeriesConfig {
    withInnerLabelProperty(property: string): EventSeriesConfig;
    withChangingLabelColor(boolProperty: string, colorHexCode: string): EventSeriesConfig 
    withEventTooltipText(text: string): EventSeriesTooltipConfig;
    finishEventSeriesConfiguration(): OptionalConfiguration;
}

interface EventSeriesTooltipConfig {
    showAlways(): EventSeriesConfig;
}

export { lineSeriesBuilder as createLineSeriesBuilder }
