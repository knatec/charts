import React from 'react';
import styled from 'styled-components';
import { Row, Col, Button } from "react-bootstrap";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { points, processRanges, startDate } from "../data/fake.data";
import ChartBuilder from './ChartBuilder';
import { createYAxisBuilder as yAxisBuilder, createYAxisBuilderFromAxis } from './YAxisBuilder';
import { createXAxisBuilder as xAxisBuilder, createXAxisBuilderFromAxis } from './XAxisBuilder';
import { createLineSeriesBuilder as lineSeriesBuilder } from './LineSeriesBuilder';
import { Range } from "../components/XAxisBuilder";

am4core.useTheme(am4themes_animated);

class AmCharts extends React.Component {

  private chart?: am4charts.XYChart;

  componentDidMount() {
    let chart = am4core.create("chartdiv", am4charts.XYChart)
    const chartBuilder = new ChartBuilder(chart);

    this.chart = chartBuilder
      .withLegend("top")
      .withXYCursor("zoomXY")
      .withData(points)
      .withXZoombar()
      .withYZoombar()
      .build();


    const xAxis = xAxisBuilder(chart)
      .newDateAxis()
      .withTitle("Zeit")
      .withInterval({
        "timeUnit": "second",
        "count": 1
      })
      .withReferenceLine(new Date(startDate.getTime() - (12 * 60 * 1000)), "#000")
      .getAxis();

    const logarithmic = yAxisBuilder(chart)
        .newValueAxis("Log Scale")
        .applyLineSeries(
          lineSeriesBuilder()
            .withName("Log Scale Legend")
            .withColor("#53917E")
            .withValueXProperty("time")
            .withValueYProperty("logValue")
            .withTooltipText("{valueY}")
            .useSeriesColorForTooltip()
            .asDottedLine()
            .getSeries()
        )
        .applySeriesColorToAxis()
        .asLogarithmic()
        .withNumberFormat("#,### a")
        .getAxis();

     const temperatureSeries = lineSeriesBuilder()
      .withName("Temperatur [[°C]]")
      .withColor("#DC493A")
      .withValueXProperty("time")
      .withValueYProperty("temperature")
      .withTooltipText("{valueY.formatNumber('###')} °C")
      .useSeriesColorForTooltip()
      .asDashDottedLine()
      .getSeries();

    const temperature = yAxisBuilder(chart)
      .newValueAxis("Temperatur [[°C]]", 1400, 2000)
      .applyLineSeries(temperatureSeries)
      .applySeriesColorToAxis()
      .withChangingLineColor(1570, "#A02C23", 1600, "#A02C23")
      .withReferenceLine(1600, "#A02C23")
      .withReferenceLine(1570, "#345995")
      .applyLineSeries(
        lineSeriesBuilder()
          .withName("Materialzugabe [[kg]]")
          .withColor("#5398BE")
          .withValueXProperty("time")
          .withValueYProperty("y")
          .asEventSeries(am4core.Rectangle)
          .withEventTooltipText("{event}")
          .showAlways()
          .withInnerLabelProperty("number")
          .withChangingLabelColor("online", "#000")
          .finishEventSeriesConfiguration()
          .getSeries()
      )
      .doNotSetAxisColor()
      .getAxis();

    const power = yAxisBuilder(chart)  
        .newValueAxis("Wirkleistung [[MW]]", 0, 12)
        .applyLineSeries(
          lineSeriesBuilder()
            .withName("Wirkleistung [[MW]]")
            .withColor("#FF9914")
            .withValueXProperty("time")
            .withValueYProperty("power")
            .withTooltipText("{valueY.formatNumber('#.00')} MW")
            .useSeriesColorForTooltip()
            .asThickLine()
            .asDashedLine()
            .getSeries()
        )
        .applySeriesColorToAxis()
        .onOpposite()
        .withNumberFormat("#.00")
        .getAxis();

    // Add keydown and keyup behavior to change the cursor behavior between zoomXY and panXY as it is very annoying not being able to pan the chart after zoom.
    document.addEventListener("keydown", this.handleStrgKeyDown, false);
    document.addEventListener("keyup", this.handleStrgKeyUp, false);

  }

  private handleStrgKeyUp = (event: any) => {
    if(event.keyCode === 17) {
      this.chart!.cursor.behavior = "zoomXY";
    }
  }

  private handleStrgKeyDown = (event: any) => {
    if(event.keyCode === 17){
      this.chart!.cursor.behavior = "panXY";
    }
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  removeAxesRanges = () => {
    const dateAxis = this.chart!.xAxes.values[0];
    dateAxis.axisRanges.clear();
  }

  addAxesRanges = () => {
    const dateAxis = this.chart!.xAxes.values[0] as am4charts.DateAxis;
    
    createXAxisBuilderFromAxis(dateAxis)
      .withRanges(processRanges);
  }

  moveReferenceLines = () => {
    const dateAxis = this.chart!.xAxes.values[0] as am4charts.DateAxis;
    dateAxis.axisRanges.clear();
    createXAxisBuilderFromAxis(dateAxis)
      .withReferenceLine(new Date(startDate.getTime() - (9 * 60 * 1000)), "#000");

    const temperatureAxis = this.chart!.yAxes.values.filter(x => x.title.text === "Temperatur [[°C]]")[0] as am4charts.ValueAxis;
    temperatureAxis.axisRanges.clear();
    createYAxisBuilderFromAxis(temperatureAxis)
      .withReferenceLine(1500, "#A02C23")
  }

  render() {
    return <div>
      <Row>
      <Col>
        <h3>Axes Ranges</h3>
        <CustomButton onClick={this.addAxesRanges}>Add</CustomButton>
        <CustomButton className="ml-2" onClick={this.removeAxesRanges}>Remove</CustomButton>
        <CustomButton className="ml-2" onClick={this.moveReferenceLines}>UpateRefLines</CustomButton>
      </Col>
    </Row>
    <Row>
      <Col>
        <Graph id="chartdiv"></Graph>
      </Col>
    </Row>
    </div>
  }
}

const CustomButton = styled.button`
  background: rgba(215, 129, 106, 0.2);
  border-color: rgba(215, 129, 106, 0.2);
  border-radius: 0.25rem;
  padding: 0.375rem 0.75rem;
  -webkit-appearance: button;
  outline: none;
  transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  &:hover {
    background: rgba(215, 129, 106, 0.3);
    border-color: rgba(215, 129, 106, 0.3);
  }
`;

const Graph = styled.div`
  height: 800px;
`

export default AmCharts;